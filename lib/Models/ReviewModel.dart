class ReviewModel {
  String heading, description, personName, date;
  double rating;

  ReviewModel(
      this.heading, this.description, this.personName, this.date, this.rating); 
}
