import 'package:app/Utills/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class CustomDrawer extends StatefulWidget {

  final drawerItems = [
    new DrawerItem("Notifications", "Assets/notification.svg"),
    new DrawerItem("Order Summary", "Assets/cart.svg"),
    new DrawerItem("Favorites", "Assets/fvrt.svg"),
    new DrawerItem("Terms & Conditions", "Assets/terms.svg"),
    new DrawerItem("Privacy Policy", "Assets/privacy.svg"),
    new DrawerItem("Settings", "Assets/settings.svg"),
    new DrawerItem("Suggestions", "Assets/contactus.svg"),
    new DrawerItem("Sign Out", "Assets/logout.svg"),

  ];

  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  int selectedIndex = 0;
  int _selectedDrawerIndex = 0;
  int _index = 0;
  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];

    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      drawerOptions.add(
        Column(
          children: <Widget>[
            ListTile(
              contentPadding:
              EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
              dense: true,
              //contentPadding: EdgeInsets.all(0.0),
              title: Text(
                '${d.title}',
                style:
                TextStyle(color: i != _index ? Colors.black : Colors.black),
              ),
              selected: i == _index,
              leading: Container(
                width: 25,
                height: 25,
                child: SvgPicture.asset(

                  d.iconData,
                  color: colorMain,
                ),
              ),
              onTap: () {
                Navigator.of(context).pop();
                openDrwawerMenuScreens(i);
              },
            ),
            Divider(
              height: 1,
            ),
          ],
        ),
      );
    }
    return Drawer(
      child: ListView(
        padding: new EdgeInsets.all(0.0),

        children: <Widget>[
          DrawerHeader(
            margin: EdgeInsets.only(bottom: 0.0),

            decoration: BoxDecoration(
              color: colorMain,
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(2.0)),
                  ClipRRect(
                      borderRadius: BorderRadius.circular(80.0),
                      child: Image.asset("Assets/avatar.png",
                        width: 80,
                        height: 80,)),
                  Padding(
                      padding: EdgeInsets.only(left: 5.0, right: 5.0)),
                  Text(
                    "Muhammad Masood",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold),
                  ),
                  Text("m.masood417@gmail.com", style: TextStyle(color: Colors.white)),
                ],
              ),
            ),

          ),

          new Column(children: drawerOptions)

        ],
      ),
    );
  }

  void openDrwawerMenuScreens(int i) {
    switch (i) {
      case 0:
        Navigator.pushNamed(context, 'notifications');
        return;
      case 1:
        Navigator.pushNamed(context, 'myorders');
        return;
      case 5:
        Navigator.pushNamed(context, 'settings');

    }
  }
}
class DrawerItem {
  String title;
  String icon;
  String iconData;
  DrawerItem(this.title, this.iconData /*this.icon*/);
}