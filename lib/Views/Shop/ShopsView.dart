import 'dart:async';
import 'package:app/Utills/constants.dart';
import 'package:app/Views/Shop/ShopProducts.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:carousel_slider/carousel_slider.dart';

import 'package:intl/intl.dart';

class PostWidget extends StatefulWidget {

  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> {
  final StreamController<void> _doubleTapImageEvents =
  StreamController.broadcast();
  bool _isSaved = false;
  int _currentImageIndex = 0;
  bool isLikedByCurrentId = false;
  int i = 0;
  var posteCreatedTime;
  var pageType;

  @override
  void initState() {
    super.initState();

  }


  @override
  void dispose() {
    _doubleTapImageEvents.close();
    super.dispose();
  }

  void _updateImageIndex(int index) {
    setState(() => _currentImageIndex = index);
  }



  void _onDoubleTapLikePhoto() {
    _toggleIsLiked();
    // setState(() => widget.post.addLikeIfUnlikedFor(currentUser));
    _doubleTapImageEvents.sink.add(null);
  }

  void _toggleIsLiked() {
    setState(() {
      if (isLikedByCurrentId)
        isLikedByCurrentId = false;
      else
        isLikedByCurrentId = true;
    });

  }



  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        // User Details
        // Photo Carosuel
        GestureDetector(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return ShopProducts();
            }));
          },
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              CarouselSlider(
                items: [
                     'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/amazon-rivet-furniture-1533048038.jpg'
                ].map((url) {
                  return CachedNetworkImage(
                      fit: BoxFit.cover,
                      width: MediaQuery.of(context).size.width,
                      imageUrl: url,
                      placeholder: (context, url) =>
                          dummyCoverImage(context, true),
                      errorWidget: (context, url, error) =>
                          dummyCoverImage(context, false));
                  /*  return Image.network(
                    url,
                    fit: BoxFit.fill,
                    width: MediaQuery.of(context).size.width,
                  ); */
                }).toList(),
                viewportFraction: 1.0,
                enableInfiniteScroll: false,
                onPageChanged: _updateImageIndex,
              ),

            ],
          ),
          onDoubleTap: _onDoubleTapLikePhoto,
        ),
        propertyListDesign(),

      ],
    );
  }
}

class PhotoCarouselIndicator extends StatelessWidget {
  final int photoCount;
  final int activePhotoIndex;

  PhotoCarouselIndicator({
    @required this.photoCount,
    @required this.activePhotoIndex,
  });

  Widget _buildDot({bool isActive}) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 3.0, right: 3.0),
        child: Container(
          height: isActive ? 7.5 : 6.0,
          width: isActive ? 7.5 : 6.0,
          decoration: BoxDecoration(
            color: isActive ? Colors.blue : Colors.grey,
            borderRadius: BorderRadius.circular(4.0),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: List.generate(photoCount, (i) => i)
          .map((i) => _buildDot(isActive: i == activePhotoIndex))
          .toList(),
    );
  }
}

propertyListDesign() {
  return Container(
    color: smokeybgColor,
    margin: EdgeInsets.zero,
    child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(0.0, 0.0, 2.0, 0.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Flexible(
                          child: Text(
                            "My Shop Title",
                            style: TextStyle(fontWeight: FontWeight.w600),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            softWrap: false,
                          ),
                        ),
                      ),
                      Container(
                        child: Flexible(
                          child: Text(
                            dummytxt,
                            style:
                            TextStyle(fontSize: 9, color: Colors.black45),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            softWrap: false,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 0.0),
                        child: Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.zero,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3.0),
                                color: colorMain,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Text("type",
                                  softWrap: false,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 9,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 3.0),
                              child: Row(
                                children: <Widget>[
                                  SvgPicture.asset(
                                    "Assets/location_line.svg",
                                    color: colorMain,
                                    allowDrawingOutsideViewBox: true,
                                    height: 15,
                                  ),
                                  Text(
                                    'open',
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    softWrap: false,
                                    style: TextStyle(
                                        fontSize: 10, color: Colors.black87),
                                  ),
                                  Text(
                                    'close',
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    softWrap: false,
                                    style: TextStyle(
                                        fontSize: 10, color: Colors.black87),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SvgPicture.asset(
                              "Assets/location_line.svg",
                              allowDrawingOutsideViewBox: true,
                              height: 15,
                              color: colorMain,
                            ),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 4.0),
                                child: Text(
                                  "Gulshan e Iqbal karachi Pakistan",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize: 10, color: Colors.black87),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        Divider(height: 1, color: colorDivider)
      ],
    ),
  );
}


