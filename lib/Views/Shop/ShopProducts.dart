import 'package:app/Models/MedicineModel.dart';
import 'package:app/Models/MyPetsModel.dart';
import 'package:app/Models/PetModel.dart';
import 'package:app/Utills/constants.dart';
import 'package:app/Views/AddPet.dart';
import 'package:app/Views/Shop/ProductListPage.dart';
import 'package:app/Views/Shop/ProductDetails.dart';
import 'package:app/Views/SideBarMenu.dart/SideBarMenu.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class ShopProducts extends StatefulWidget {
  ShopProducts({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _ShopProductsState createState() => _ShopProductsState();
}

class _ShopProductsState extends State<ShopProducts> {
  List<PetModel> widgetList = new List();
  List<MedicineModel> medicineList = new List();
  List<MyPets> myPets = new List();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    widgetList.add(new PetModel("Dog", "Assets/dog.png", Color(0xFFc9f5f2)));
    widgetList.add(new PetModel("Dog", "Assets/dog.png", Color(0xFFc9f5f2)));
    widgetList.add(new PetModel("Dog", "Assets/dog.png", Color(0xFFc9f5f2)));
    widgetList.add(new PetModel("Dog", "Assets/dog.png", Color(0xFFc9f5f2)));
    widgetList.add(new PetModel("Dog", "Assets/dog.png", Color(0xFFc9f5f2)));

    medicineList.add(new MedicineModel(
        "American Journey", "Assets/royalcaninLOGO.png", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/royalcaninLOGO.png", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/royalcaninLOGO.png", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/royalcaninLOGO.png", "Salmon & Sw...", 39.99, 4));

    myPets.add(new MyPets(
        "Jerry", "Assets/myDog.jpg", "Pomeranian", "Male", 3, 4.6, true));
    myPets.add(new MyPets(
        "Jerry", "Assets/myDog.jpg", "Pomeranian", "Male", 3, 4.6, true));

    // TODO: implement initState
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    /*24 is for notification bar on Android*/
    /*24 is for notification bar on Android*/
    return  Scaffold(
      backgroundColor: Colors.white,
        appBar: AppBar(

          backgroundColor: colorMain,
          actions: actionWidgets(context),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0),
                        child: Container(
                          padding: EdgeInsets.all(5),
                          width: MediaQuery.of(context).size.width - 30,
                          height: 45,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(0.0, 5.0), //(x,y)
                                  blurRadius: 15.0,
                                )
                              ]),
                          child: TextField(
                            maxLines: 1,
                            decoration: InputDecoration(
                                hintText: "Search Shops",
                                hintStyle:
                                    TextStyle(color: Color(0xFFb3b3b3)),
                                prefixIcon: Icon(Icons.search),
                                border: InputBorder.none),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),


                Padding(padding: EdgeInsets.only(top: 20)),

                Container(
                  child: new StaggeredGridView.countBuilder(
                    shrinkWrap: true,
                    controller: new ScrollController(keepScrollOffset: false),
                    crossAxisCount: 4,
                    itemCount: medicineList.length,
                    itemBuilder: (BuildContext context, index) {
                      List<String> price =
                          medicineList[index].price.toString().split(".");
                      String priceBeforeDecimel = price[0];
                      String priceAfterDecimel = price[1];

                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ProductDetail()));
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            padding: const EdgeInsets.only(
                                bottom: 10.0, left: 10, right: 10),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(0.0, 3.0), //(x,y)
                                    blurRadius: 10.0,
                                  )
                                ]),
                            child: Column(
                              children: <Widget>[
                                Image.asset(
                                  medicineList[index].image,
                                  width: 100,
                                  height: 130,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    medicineList[index].name,
                                    style: TextStyle(
                                        fontFamily: "AxiformaBold",
                                        fontSize: Constants().fontSize13,
                                        fontWeight: FontWeight.bold,
                                        color: Constants().fontBlack),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    medicineList[index].brand,
                                    style: TextStyle(
                                        fontFamily: "AxiformaRegular",
                                        fontSize: Constants().fontSize11,
                                        color: Constants().fontBlack),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.bottomLeft,
                                  child: RatingBarIndicator(
                                    rating: medicineList[index].ratings,
                                    itemBuilder: (context, index) => Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                    ),
                                    itemCount: 5,
                                    itemSize: 15.0,
                                    direction: Axis.horizontal,
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            priceBeforeDecimel,
                                            style: TextStyle(
                                                fontFamily: "AxiformaBold",
                                                color: Constants().fontOrange,
                                                fontSize:
                                                    Constants().fontSize25),
                                          ),
                                          Text(
                                            "." + priceAfterDecimel,
                                            style: TextStyle(
                                                fontFamily: "AxiformaBold",
                                                color: Constants().fontOrange,
                                                fontSize:
                                                    Constants().fontSize15),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Text(
                                          "Free 1-2 days",
                                          style: TextStyle(
                                              fontFamily: "AxiformaRegular",
                                              color: Constants().fontBlack,
                                              fontWeight: FontWeight.bold,
                                              fontSize:
                                                  Constants().fontSize11 - 1),
                                        ),
                                        Text("Shipping",
                                            style: TextStyle(
                                                fontFamily: "AxiformaRegular",
                                                color: Constants().fontBlack,
                                                fontWeight: FontWeight.bold,
                                                fontSize:
                                                    Constants().fontSize11 - 1))
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    staggeredTileBuilder: (int index) =>
                        new StaggeredTile.count(2, index.isEven ? 2.75 : 2.8),
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    height: 80,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(colors: [
                          Constants().gradientStart,
                          Constants().gradientEnd,
                        ]),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: Row(
                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                              color: Constants().buttonLightBlue,
                            ),
                            child: Row(
                              children: <Widget>[
                                RotatedBox(
                                    quarterTurns: -1,
                                    child: Text(
                                      "SAVE",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "PoppinsRegular",
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )),
                                Text("30%",
                                    style: TextStyle(
                                        color: Constants().percentageOrange,
                                        fontFamily: "PoppinsBold",
                                        fontSize: 35,
                                        fontWeight: FontWeight.bold))
                              ],
                            )),
                        Padding(
                          padding: EdgeInsets.only(left: 10),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text(
                                  "On your first ",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: "PoppinsRegular",
                                      fontSize: Constants().fontSize15,
                                      fontWeight: FontWeight.bold),
                                ),
                                Image.asset(
                                  "Assets/waste.png",
                                  height: 15,
                                  width: 15,
                                ),
                                Text(
                                  " Order",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: "PoppinsRegular",
                                      fontSize: Constants().fontSize15,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Text(
                              "Learn More",
                              style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  color: Color(0xFF9b96c6),
                                  fontFamily: "PoppinsRegular",
                                  fontSize: Constants().fontSize15),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  child: new StaggeredGridView.countBuilder(
                    shrinkWrap: true,
                    controller: new ScrollController(keepScrollOffset: false),
                    crossAxisCount: 4,
                    itemCount: medicineList.length,
                    itemBuilder: (BuildContext context, index) {
                      List<String> price =
                      medicineList[index].price.toString().split(".");
                      String priceBeforeDecimel = price[0];
                      String priceAfterDecimel = price[1];

                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ProductDetail()));
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            padding: const EdgeInsets.only(
                                bottom: 10.0, left: 10, right: 10),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                BorderRadius.all(Radius.circular(5)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(0.0, 3.0), //(x,y)
                                    blurRadius: 10.0,
                                  )
                                ]),
                            child: Column(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.bottomRight,
                                  child: Image.asset(
                                    "Assets/rxLOGO.png",
                                    height: 20,
                                    width: 20,
                                  ),
                                ),
                                Image.asset(
                                  medicineList[index].image,
                                  width: 100,
                                  height: 100,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    medicineList[index].name,
                                    style: TextStyle(
                                        fontFamily: "AxiformaBold",
                                        fontSize: Constants().fontSize13,
                                        fontWeight: FontWeight.bold,
                                        color: Constants().fontBlack),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    medicineList[index].brand,
                                    style: TextStyle(
                                        fontFamily: "AxiformaRegular",
                                        fontSize: Constants().fontSize11,
                                        color: Constants().fontBlack),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.bottomLeft,
                                  child: RatingBarIndicator(
                                    rating: medicineList[index].ratings,
                                    itemBuilder: (context, index) => Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                    ),
                                    itemCount: 5,
                                    itemSize: 15.0,
                                    direction: Axis.horizontal,
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            priceBeforeDecimel,
                                            style: TextStyle(
                                                fontFamily: "AxiformaBold",
                                                color: Constants().fontOrange,
                                                fontSize:
                                                Constants().fontSize25),
                                          ),
                                          Text(
                                            "." + priceAfterDecimel,
                                            style: TextStyle(
                                                fontFamily: "AxiformaBold",
                                                color: Constants().fontOrange,
                                                fontSize:
                                                Constants().fontSize15),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Text(
                                          "Free 1-2 days",
                                          style: TextStyle(
                                              fontFamily: "AxiformaRegular",
                                              color: Constants().fontBlack,
                                              fontWeight: FontWeight.bold,
                                              fontSize:
                                              Constants().fontSize11 - 1),
                                        ),
                                        Text("Shipping",
                                            style: TextStyle(
                                                fontFamily: "AxiformaRegular",
                                                color: Constants().fontBlack,
                                                fontWeight: FontWeight.bold,
                                                fontSize:
                                                Constants().fontSize11 - 1))
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    staggeredTileBuilder: (int index) =>
                    new StaggeredTile.count(2, index.isEven ? 2.75 : 2.8),
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                  ),
                ),
              ],
            ),
          ),
        ),
      
    );
  }

  headingAndValue(String heading, String value) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          heading,
          style: TextStyle(
              color: Constants().fontBlack,
              fontFamily: "AxiformaRegular",
              fontSize: Constants().fontSize11),
        ),
        Text(
          value,
          style: TextStyle(
              color: Constants().fontBlack,
              fontFamily: "AxiformaRegular",
              fontSize: Constants().fontSize13,
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}
