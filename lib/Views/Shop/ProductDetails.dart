import 'package:app/Models/MedicineModel.dart';
import 'package:app/Models/QandAModel.dart';
import 'package:app/Models/ReviewModel.dart';
import 'package:app/Utills/constants.dart';
import 'package:app/Views/AllQuestions.dart';
import 'package:app/Views/Shop/AllReviews.dart';
import 'package:app/Views/AskQuestion.dart';
import 'package:app/Views/Shop/writeReview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

int quantity = 0;

class ProductDetail extends StatefulWidget {
  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  List<MedicineModel> medicineList = new List();
  List<ReviewModel> reviewsList = new List();

  List<QstnAsnwrModel> qList = new List();

  List<String> strengths = ['30 grams', '60 grams', '90 grams', '120 grams'];
  String _selectedStrength;

  List<String> count = [
    '30 Tablets',
    '60 Tablets',
    '90 Tablets',
    '120 Tablets'
  ];
  String _selectedCount;

  List<String> autoshipSpan = [
    'Every 1 week',
    'Every 2 week',
    'Every 3 week',
    'Every 4 week',
  ];
  String _selectedAutoshipPlan;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));

    qList.add(new QstnAsnwrModel(
        "Where is this food made ?",
        "The American Journey Grain-free line is produced in Kansas while the new American Journey Limited Ingredient Grain-Free line is produced in California",
        "29 June 2020"));
    qList.add(new QstnAsnwrModel(
        "Where is this food made ?",
        "The American Journey Grain-free line is produced in Kansas while the new American Journey Limited Ingredient Grain-Free line is produced in California",
        "29 June 2020"));
    qList.add(new QstnAsnwrModel(
        "Where is this food made ?",
        "The American Journey Grain-free line is produced in Kansas while the new American Journey Limited Ingredient Grain-Free line is produced in California",
        "29 June 2020"));
    qList.add(new QstnAsnwrModel(
        "Where is this food made ?",
        "The American Journey Grain-free line is produced in Kansas while the new American Journey Limited Ingredient Grain-Free line is produced in California",
        "29 June 2020"));

    reviewsList.add(new ReviewModel(
        "Great food",
        "Otis is a picky eater. It’s been hard finding a good food that he will eat. He absolutely love this new food. Never would have known about it if his regular food was out of stock. His vet recommended his old food.",
        "Otis",
        "29 June, 2020",
        5.0));
    reviewsList.add(new ReviewModel(
        "Great food",
        "Otis is a picky eater. It’s been hard finding a good food that he will eat. He absolutely love this new food. Never would have known about it if his regular food was out of stock. His vet recommended his old food.",
        "Otis",
        "29 June, 2020",
        5.0));
    reviewsList.add(new ReviewModel(
        "Great food",
        "Otis is a picky eater. It’s been hard finding a good food that he will eat. He absolutely love this new food. Never would have known about it if his regular food was out of stock. His vet recommended his old food.",
        "Otis",
        "29 June, 2020",
        5.0));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customeAppbar("Details", context),
      body: SafeArea(
          child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Constants().greyColorAppBar,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Stack(
                  children: <Widget>[
                    Align(
                        alignment: Alignment.topCenter,
                        child: Column(
                          children: <Widget>[
                            Image.asset("Assets/galliprant.png",
                            height: 190,),
                            Padding(padding: EdgeInsets.only(bottom: 10)),
                          ],
                        )),

                    Align(
                        alignment: Alignment.topRight,
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  color: Colors.white, shape: BoxShape.circle),
                              child: Icon(Icons.favorite_border),
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 10),
                            ),


                          ],
                        ))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Text(
                  "Beats Studio3 Wireless headphone 1",
                  style: TextStyle(
                      fontFamily: "AxiformaBold",
                      color: Colors.black,
                      fontSize: Constants().fontSize18),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "By Beats",
                      style: TextStyle(
                          fontFamily: "AxiformaRegular",
                          color: Constants().fontBlack,
                          decoration: TextDecoration.underline),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        RatingBarIndicator(
                          rating: 4.0,
                          itemBuilder: (context, index) => Icon(
                            Icons.star,
                            color: colorMain,
                          ),
                          itemCount: 5,
                          itemSize: 15.0,
                          direction: Axis.horizontal,
                          unratedColor: Colors.grey,
                        ),
                        Text(
                          "67 Reviews",
                          style: TextStyle(
                              decoration: TextDecoration.underline,
                              fontFamily: "AxiformaRegular",
                              fontSize: Constants().fontSize11),
                        )
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Original Price",
                              style: TextStyle(
                                color: Constants().fontBlack,
                                fontFamily: "AxiformaRuglar",
                                fontSize: Constants().fontSize13,
                              )),
                          Row(
                            children: <Widget>[
                              Text(
                                "800",
                                style: TextStyle(
                                    color: Constants().fontBlack,
                                    fontFamily: "AxiformaBold",
                                    fontSize: 13,
                                    decoration: TextDecoration.lineThrough),
                              ),
                              SizedBox(width: 5,),
                              Text("AED",
                                  style: TextStyle(
                                      fontFamily: "AxiformaRegular",
                                      color: Constants().fontBlack,
                                      fontWeight: FontWeight.bold,
                                      fontSize:
                                      Constants().fontSize13 - 1))
                            ],
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 5),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            " 650",
                            style: TextStyle(
                                fontFamily: "AxiformaBold",
                                fontSize: Constants().fontSize25,
                                color: Constants().fontOrange),
                          ),
                          SizedBox(width: 5,),
                          Text("AED",
                              style: TextStyle(
                                  fontFamily: "AxiformaRegular",
                                  color: Constants().fontBlack,
                                  fontWeight: FontWeight.bold,
                                  fontSize:
                                  Constants().fontSize15 - 1))
                        ],
                      ),

                    ],
                  )),
              Padding(
                  padding: EdgeInsets.only(
                bottom: 20,
              )),
              Container(
                height: 90,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: 20,
                  itemBuilder: (BuildContext context, index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5.0),
                      child: Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width /
                            4.7,
                        padding: EdgeInsets.symmetric(
                          vertical: 10,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                              Radius.circular(5)),
                        ),
                        child: Image.asset(
                          "Assets/galliprant.png",
                          height: 40,
                          width: 40,
                        ),
                      ),
                    );
                  },
                ),
              ),
              Container(
                  padding: EdgeInsets.only(top: 10),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Constants().greyColorAppBar,
                  ),
                  child: Column(
                    children: <Widget>[

                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Container(
                            width: double.infinity,
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 10),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(0.0, 5.0), //(x,y)
                                    blurRadius: 15.0,
                                  )
                                ]),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Quantity:   ",
                                  style: TextStyle(
                                      fontFamily: "AxiformaRegular",
                                      fontSize: Constants().fontSize15,
                                      color: Constants().fontBlack),
                                ),
                                Expanded(
                                  child: Text(
                                    "$quantity",
                                    style: TextStyle(
                                        fontFamily: "AxiformaBold",
                                        fontSize: Constants().fontSize18,
                                        color: Constants().fontBlack),
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        quantity++;
                                        setState(() {});
                                      },
                                      child: Image.asset(
                                        "Assets/arrowUP.png",
                                        height: 15,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 5),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        if (quantity > 0) {
                                          quantity--;
                                          setState(() {});
                                        }
                                      },
                                      child: Image.asset(
                                        "Assets/arrowDOWN.png",
                                        height: 15,
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 10),
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 7, horizontal: 15),
                                  decoration: BoxDecoration(
                                    color: colorMain,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(5),
                                    ),
                                  ),
                                  child: Center(
                                      child: Text("Add to Cart",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: "AxiformaBold",
                                              fontSize:
                                                  Constants().fontSize13))),
                                )
                              ],
                            )),
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 20)),
                      Container(
                        height: 175,
                        width: MediaQuery.of(context).size.width,
                        child:tabBarContainer()
                      ),

                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                "Customer Reviews",
                                style: TextStyle(
                                    fontFamily: "AxiformaBold",
                                    fontSize: Constants().fontSize18,
                                    color: Colors.black),
                              ),
                            ),
                            GestureDetector(
                              onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          AllReviews())),
                              child: Text("See All",
                                  style: TextStyle(
                                      fontFamily: "AxiformaRegular",
                                      fontSize: Constants().fontSize15,
                                      color: Constants().fontBlack,
                                      decoration:
                                      TextDecoration.underline)),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10.0),
                          child: Row(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding:
                                const EdgeInsets.only(left: 10),
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          "4.4",
                                          style: TextStyle(
                                            fontFamily: "AxiformaBold",
                                            color:
                                            Constants().fontBlack,
                                            fontSize: 30,
                                          ),
                                        ),
                                        Text("/5",
                                            style: TextStyle(
                                              fontFamily:
                                              "AxiformaBold",
                                              color: Constants()
                                                  .greyColorLabel,
                                              fontSize: Constants()
                                                  .fontSize25,
                                            ))
                                      ],
                                    ),
                                    RatingBarIndicator(
                                      rating: 4.4,
                                      itemBuilder: (context, index) =>
                                          Icon(
                                            Icons.star,
                                            color: colorMain,
                                          ),
                                      itemCount: 5,
                                      itemSize: 22.0,
                                      direction: Axis.horizontal,
                                    ),
                                    Text("275 Rating",
                                        style: TextStyle(
                                          fontFamily: "AxiformaRegular",
                                          color: Constants()
                                              .greyColorLabel,
                                          fontSize:
                                          Constants().fontSize18,
                                        ))
                                  ],
                                ),
                              ),
                              Container(
                                width:
                                MediaQuery.of(context).size.width /
                                    1.75,
                                child: Column(
                                  children: <Widget>[
                                    startPercentage("5", 0.73),
                                    startPercentage("4", 0.12),
                                    startPercentage("3", 0.07),
                                    startPercentage("2", 0.04),
                                    startPercentage("1", 0.05),
                                  ],
                                ),
                              )
                            ],
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 5)),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          height: 238,
                          child: ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: reviewsList.length,
                              itemBuilder:
                                  (BuildContext cntxt, int index) {
                                return reviewTile(
                                    reviewsList[index].heading,
                                    reviewsList[index].description,
                                    reviewsList[index].personName,
                                    reviewsList[index].date,
                                    reviewsList[index].rating,
                                    context);
                              })),
                      Padding(padding: EdgeInsets.only(bottom: 5)),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10.0),
                        child: GestureDetector(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      WriteReviewScreen())),
                          child: Container(
                              width: MediaQuery.of(context).size.width,
                              padding:
                              EdgeInsets.symmetric(vertical: 10),
                              decoration: BoxDecoration(


                                  border: Border.all(color: Colors.black,
                                      width: 2),
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10))),
                              child: Center(
                                child: Text(
                                  "Write a Review",
                                  style: TextStyle(
                                      fontFamily: "AxiformaBold",
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                      fontSize: Constants().fontSize18),
                                ),
                              )),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 10)),

                    ],
                  )),
            ],
          ),
        ),
      )),
    );
  }

  dropDownMenu(
      List<String> list, BuildContext context, bool isStrength, bool isCount) {
    return DropdownButtonHideUnderline(
      child: DropdownButton(
        // underline: SizedBox(),

        hint: Text(
            isStrength == true
                ? 'Select Strength'
                : isCount == true ? "Select Count" : "Autoship Plan",
            style: TextStyle(fontSize: 15)), // Not necessary for Option 1
        value: isStrength == true
            ? _selectedStrength
            : isCount == true ? _selectedCount : _selectedAutoshipPlan,
        onChanged: (newValue) {
          setState(() {
            if (isStrength && !isCount) {
              _selectedStrength = newValue;
            }
            if (!isStrength && isCount) {
              _selectedCount = newValue;
            }
            if (!isStrength && !isCount) {
              _selectedAutoshipPlan = newValue;
            }
          });
        },
        items: list.map((location) {
          return DropdownMenuItem(
            child: new Text(location),
            value: location,
          );
        }).toList(),
      ),
    );
  }

  tabBarContainer() {
    return Container(
      color: Constants().greyColorAppBar,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Description",
            style: TextStyle(
                fontFamily: "AxiformaBold",
                fontWeight: FontWeight.bold,
                fontSize: Constants().fontSize18,
                color: Colors.black),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 5),
          ),
          Text(
              "American Journey is full of nutritious ingredients that fuel your pet’s everyday adventures. The Beef & Sweet Potato Recipe Grain-Free Dry Dog Food formula features real Read More",
              style: TextStyle(
                  fontFamily: "AxiformaRegular",
                  fontWeight: FontWeight.w500,
                  fontSize: Constants().fontSize15)),
          Padding(
            padding: EdgeInsets.only(bottom: 10),
          ),
        ],
      ),
    );
  }


  listViewTile(
    String image,
    String name,
    String brand,
    String priceBeforeDecimel,
    String priceAfterDecimel,
    double rating,
  ) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ProductDetail()));
      },
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          padding:
              const EdgeInsets.only(bottom: 10.0, right: 10, left: 10, top: 2),
          height: 250,
          width: 160,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 3.0), //(x,y)
                  blurRadius: 10.0,
                )
              ]),
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.bottomRight,
                child: Image.asset(
                  "Assets/rxLOGO.png",
                  height: 20,
                  width: 20,
                ),
              ),
              Image.asset(
                image,
                width: 100,
                height: 100,
              ),
              Padding(
                padding: EdgeInsets.only(top: 5),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  name,
                  style: TextStyle(
                      fontFamily: "AxiformaBold",
                      fontSize: Constants().fontSize13,
                      fontWeight: FontWeight.bold,
                      color: Constants().fontBlack),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  brand,
                  style: TextStyle(
                      fontFamily: "AxiformaRegular",
                      fontSize: Constants().fontSize11,
                      color: Constants().fontBlack),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: RatingBarIndicator(
                  rating: rating,
                  itemBuilder: (context, index) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  itemCount: 5,
                  itemSize: 15.0,
                  direction: Axis.horizontal,
                ),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Row(
                      children: <Widget>[
                        Text(
                          priceBeforeDecimel,
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              color: Constants().fontOrange,
                              fontSize: Constants().fontSize25),
                        ),
                        Text(
                          "." + priceAfterDecimel,
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              color: Constants().fontOrange,
                              fontSize: Constants().fontSize15),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        "Free 1-2 days",
                        style: TextStyle(
                            fontFamily: "AxiformaRegular",
                            color: Constants().fontBlack,
                            fontWeight: FontWeight.bold,
                            fontSize: Constants().fontSize11 - 1),
                      ),
                      Text("Shipping",
                          style: TextStyle(
                              fontFamily: "AxiformaRegular",
                              color: Constants().fontBlack,
                              fontWeight: FontWeight.bold,
                              fontSize: Constants().fontSize11 - 1))
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  questionListTile(
      String question, String answer, String date, BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          padding: const EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width / 1.5,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 3.0), //(x,y)
                  blurRadius: 10.0,
                )
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                date,
                style: TextStyle(
                  fontFamily: "AxiformaRegular",
                  fontSize: Constants().fontSize13,
                  fontWeight: FontWeight.w600,
                  color: Color(0xFFBCB5B9),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 5),
              ),
              Text(
                question,
                style: TextStyle(
                    fontFamily: "AxiformaBold",
                    fontWeight: FontWeight.w600,
                    fontSize: Constants().fontSize15,
                    color: Constants().blue),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 5),
              ),
              Text(
                answer,
                style: TextStyle(
                  fontFamily: "AxiformaRegular",
                  fontSize: Constants().fontSize13,
                  fontWeight: FontWeight.w600,
                  color: Constants().fontBlack,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 5),
              ),
              Text(
                "By PharamZu.",
                style: TextStyle(
                  fontFamily: "AxiformaRegular",
                  fontSize: Constants().fontSize11,
                  fontWeight: FontWeight.w600,
                  color: Color(0xFFBCB5B9),
                ),
              ),
            ],
          ),
        ));
  }

  reviewTile(String heading, String description, String author, String date,
      double rating, BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          padding: const EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width / 1.4,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 3.0), //(x,y)
                  blurRadius: 10.0,
                )
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                heading,
                style: TextStyle(
                    fontFamily: "AxiformaBold",
                    fontWeight: FontWeight.w600,
                    fontSize: Constants().fontSize15,
                    color: colorMain),
              ),
              Row(
                children: <Widget>[
                  RatingBarIndicator(
                    rating: 4.0,
                    itemBuilder: (context, index) => Icon(
                      Icons.star,
                      color: colorMain,
                    ),
                    itemCount: 5,
                    itemSize: 15.0,
                    direction: Axis.horizontal,
                    unratedColor: Colors.grey,
                  ),
                  Text(
                    "  By " + author + " on " + date,
                    style: TextStyle(
                      fontFamily: "AxiformaRegular",
                      fontSize: Constants().fontSize13,
                      fontWeight: FontWeight.w600,
                      color: Color(0xFFBCB5B9),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 5),
              ),
              Text(
                description,
                style: TextStyle(
                  fontFamily: "AxiformaRegular",
                  fontSize: Constants().fontSize13,
                  fontWeight: FontWeight.w600,
                  color: Constants().fontBlack,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 5),
              ),
            ],
          ),
        ));
  }

  startPercentage(String starNumber, double percentage) {
    double stringPercentege = percentage * 100;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.star,
          color: colorMain,
          size: 20,
        ),
        Padding(
          padding: EdgeInsets.only(right: 5),
        ),
        Container(
          width: MediaQuery.of(context).size.width/9,
          child: Text(
            starNumber + " Star",
            style: TextStyle(
                fontFamily: "AxiformaRegular",
                fontWeight: FontWeight.w400,
                color: Constants().fontBlack,
                fontSize: Constants().fontSize13),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(right: 5),
        ),
        new LinearPercentIndicator(
          width: 100.0,
          lineHeight: 8.0,
          percent: percentage,
          progressColor: colorMain,
        ),
        Padding(
          padding: EdgeInsets.only(right: 5),
        ),
        Text(
          stringPercentege.toInt().toString() + "%",
          style: TextStyle(
              fontFamily: "AxiformaRegular",
              fontWeight: FontWeight.w400,
              color: Constants().fontBlack,
              fontSize: Constants().fontSize13),
        ),
      ],
    );
  }
}
