import 'package:app/Utills/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class AllReviews extends StatefulWidget {
  @override
  _AllReviewsState createState() => _AllReviewsState();
}

class _AllReviewsState extends State<AllReviews> {
  List<String> reviewList = new List();
  List<String> starList = new List();
  List<bool> _selected =
      List.generate(6, (i) => false); // initially fill it up with false

  @override
  void initState() {
    super.initState();

    reviewList.add("A");
    reviewList.add("A");
    reviewList.add("A");
    reviewList.add("A");

    starList.add("All");
    starList.add("1 Star Reviews");
    starList.add("2 Star Reviews");
    starList.add("3 Star Reviews");
    starList.add("4 Star Reviews");
    starList.add("5 Star Reviews");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Constants().greyColorAppBar,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Constants().fontBlack,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
            
              color: Constants().greyColorAppBar,
              child: Row(
                children: [
                  Padding(padding: EdgeInsets.only(left: 20)),
                  Text(
                    "All Customer Reviews",
                    style: TextStyle(
                      fontSize: Constants().fontSize25 - 3,
                      color: Constants().appbarTextclr,
                      fontFamily: "AxiformaBold",
                    ),
                  ),
                ],
              ),
            ),
            Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10.0, vertical: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "4.4",
                                style: TextStyle(
                                  fontFamily: "AxiformaBold",
                                  color: Constants().fontBlack,
                                  fontSize: 30,
                                ),
                              ),
                              Text("/5",
                                  style: TextStyle(
                                    fontFamily: "AxiformaBold",
                                    color: Constants().greyColorLabel,
                                    fontSize: Constants().fontSize25,
                                  ))
                            ],
                          ),
                          RatingBarIndicator(
                            rating: 4.4,
                            itemBuilder: (context, index) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            itemCount: 5,
                            itemSize: 22.0,
                            direction: Axis.horizontal,
                          ),
                          Text("275 Rating",
                              style: TextStyle(
                                fontFamily: "AxiformaRegular",
                                color: Constants().greyColorLabel,
                                fontSize: Constants().fontSize18,
                              ))
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 1.75,
                      child: Column(
                        children: <Widget>[
                          startPercentage("5", 0.73),
                          startPercentage("4", 0.12),
                          startPercentage("3", 0.07),
                          startPercentage("2", 0.04),
                          startPercentage("1", 0.05),
                        ],
                      ),
                    )
                  ],
                )),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 20,
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: starList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: GestureDetector(
                      onTap: () => setState(() => _selected[index] =
                          !_selected[index]), // reverse bool value

                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                            color: _selected[index]
                                ? Constants().blue
                                : Constants()
                                    .greyColorAppBar, // if current item is selected show blue color
                            borderRadius: BorderRadius.all(Radius.circular(20))),
                        child: Center(
                            child: Text(
                          starList[index],
                          style: TextStyle(
                              color: _selected[index]
                                  ? Colors.white
                                  : Constants().fontBlack),
                        )),
                      ),
                    ),
                  );
                },
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 10)),
            Expanded(
                          child: Container(
                width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height / 2.1,
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: reviewList.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(0.0, 3.0), //(x,y)
                                blurRadius: 10.0,
                              )
                            ],
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Great food",
                              style: TextStyle(
                                fontSize: Constants().fontSize18,
                                color: Constants().appbarTextclr,
                                fontFamily: "AxiformaBold",
                                height: 1,
                              ),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                RatingBarIndicator(
                                  rating: 4.4,
                                  itemBuilder: (context, index) => Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  ),
                                  itemCount: 5,
                                  itemSize: 15.0,
                                  direction: Axis.horizontal,
                                ),
                                Text(" " + "29 June, 2020",
                                    style: TextStyle(
                                        color: Constants().greyColorLabel,
                                        fontFamily: "AxiformaBold",
                                        height: 1,
                                        fontSize: Constants().fontSize13)),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 10),
                            ),
                            Text(
                              "Otis is a picky eater. It’s been hard finding a good food that he will eat. He absolutely love this new food. Never would have known about it if his regular food was out of stock. His vet recommended his old food. ",
                              style: TextStyle(
                                  fontFamily: "AxiformaRegular",
                                  fontWeight: FontWeight.w600,
                                  color: Constants().fontBlack),
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 20),
                            ),
                            Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 10),
                                  decoration: BoxDecoration(
                                      color: Constants().greyColorAppBar,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        "Assets/thumbsUp.png",
                                        height: 20,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left: 5),
                                      ),
                                      Text(
                                        "Helpful (12)",
                                        style: TextStyle(
                                            fontFamily: "AxiformaRegular",
                                            color: Color(0xFF626262)),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 20),
                                ),
                                Text(
                                  "Report",
                                  style: TextStyle(
                                      fontFamily: "AxiformaBold",
                                      color: Constants().greyColorLabel,
                                      fontSize: Constants().fontSize15),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  startPercentage(String starNumber, double percentage) {
    double stringPercentege = percentage * 100;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.star,
          color: Colors.amber,
          size: 20,
        ),
        Padding(
          padding: EdgeInsets.only(right: 5),
        ),
        Container(
          width: MediaQuery.of(context).size.width / 9,
          child: Text(
            starNumber + " Star",
            style: TextStyle(
                fontFamily: "AxiformaRegular",
                fontWeight: FontWeight.w400,
                color: Constants().fontBlack,
                fontSize: Constants().fontSize13),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(right: 5),
        ),
        new LinearPercentIndicator(
          width: 100.0,
          lineHeight: 8.0,
          percent: percentage,
          progressColor: Colors.orange,
        ),
        Padding(
          padding: EdgeInsets.only(right: 5),
        ),
        Text(
          stringPercentege.toInt().toString() + "%",
          style: TextStyle(
              fontFamily: "AxiformaRegular",
              fontWeight: FontWeight.w400,
              color: Constants().fontBlack,
              fontSize: Constants().fontSize13),
        ),
      ],
    );
  }
}
