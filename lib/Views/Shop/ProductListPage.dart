import 'package:app/Models/MedicineModel.dart';
import 'package:app/Utills/FilterDropDown.dart';
import 'package:app/Utills/constants.dart';
import 'package:app/Utills/multiSelectChips.dart';
import 'package:app/Views/Shop/ProductDetails.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class ProductListPage extends StatefulWidget {
  @override
  _ProductListPageState createState() => _ProductListPageState();
}

class _ProductListPageState extends State<ProductListPage> {
  GlobalKey btnKey = GlobalKey();

  List<Icon> icons = [
    Icon(Icons.person),
    Icon(Icons.settings),
    Icon(Icons.credit_card),
  ];

  List<String> _dynamicChips = [
    'Health',
    'Food',
    'Nature',
    "Form",
    "Pet Type",
    "abcd",
  ];

  List<MedicineModel> medicineList = new List();
  List<String> tags = [
    "Form",
    "Pet Type",
    "Health Condition",
    "Life Stage",
    "Form",
    "Form",
  ];
  @override
  void initState() {
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFeeeef0),
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0,
        backgroundColor: colorMain,
        title: Text("Shop Products",style: TextStyle(
          color: Colors.white,
          fontFamily: "AxiformaBold",
          fontSize: Constants().fontSize18,
        ),),
        titleSpacing: 0.0,
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
            )),
        actions: actionWidgets(context),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SafeArea(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[

            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(5),
                  child: new StaggeredGridView.countBuilder(
                    shrinkWrap: true,
                    controller: new ScrollController(keepScrollOffset: false),
                    crossAxisCount: 4,
                    itemCount: medicineList.length,
                    itemBuilder: (BuildContext context, index) {
                      List<String> price =
                          medicineList[index].price.toString().split(".");
                      String priceBeforeDecimel = price[0];
                      String priceAfterDecimel = price[1];

                      return GestureDetector(
                        onTap: (){
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ProductDetail()));
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            height: MediaQuery.of(context).size.height,
                            padding: const EdgeInsets.only(
                                bottom: 10.0, left: 10, right: 10),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(0.0, 3.0), //(x,y)
                                    blurRadius: 10.0,
                                  )
                                ]),
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: 10,),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        color: Constants().greyColorAppBar, shape: BoxShape.circle),
                                    child: Icon(Icons.favorite,
                                    color: colorMain,
                                    ),
                                  ),
                                ),
                                Image.asset(
                                  "Assets/galliprant.png",
                                  width: 100,
                                  height: 100,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Beats Studio3 Wireless headphone',
                                    style: TextStyle(
                                        fontFamily: "AxiformaBold",
                                        fontSize: Constants().fontSize13,
                                        fontWeight: FontWeight.bold,
                                        color: Constants().fontBlack),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Beats',
                                    style: TextStyle(
                                        fontFamily: "AxiformaRegular",
                                        fontSize: Constants().fontSize11,
                                        color: Constants().fontBlack),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Row(
                                    children: <Widget>[
                                      RatingBarIndicator(
                                        rating: medicineList[index].ratings,
                                        itemBuilder: (context, index) => Icon(
                                          Icons.star,
                                          color: colorMain,
                                        ),
                                        itemCount: 5,
                                        itemSize: 15.0,
                                        direction: Axis.horizontal,
                                      ),
                                      SizedBox(width: 5,),
                                      Text(
                                        '(27 Reviews)',
                                        style: TextStyle(
                                            fontFamily: "AxiformaRegular",
                                            fontSize: Constants().fontSize11,
                                            color: Constants().fontBlack),
                                      )
                                    ],
                                  ),
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text("Original Price",
                                            style: TextStyle(
                                              color: Constants().fontBlack,
                                              fontFamily: "AxiformaRuglar",
                                              fontSize: Constants().fontSize11,
                                            )),
                                        Row(
                                          children: <Widget>[
                                            Text(
                                              "800",
                                              style: TextStyle(
                                                  color: Constants().fontBlack,
                                                  fontFamily: "AxiformaBold",
                                                  fontSize: 12,
                                                  decoration: TextDecoration.lineThrough),
                                            ),
                                            SizedBox(width: 5,),
                                            Text("AED",
                                                style: TextStyle(
                                                    fontFamily: "AxiformaRegular",
                                                    color: Constants().fontBlack,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize:
                                                    Constants().fontSize11 - 1))
                                          ],
                                        ),

                                      ],
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5),
                                    ),
                                    Text(
                                      " 650",
                                      style: TextStyle(
                                          fontFamily: "AxiformaBold",
                                          fontSize: Constants().fontSize25,
                                          color: Constants().fontOrange),
                                    ),
                                    SizedBox(width: 5,),
                                    Text("AED",
                                        style: TextStyle(
                                            fontFamily: "AxiformaRegular",
                                            color: Constants().fontBlack,
                                            fontWeight: FontWeight.bold,
                                            fontSize:
                                            Constants().fontSize11 - 1))

                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    staggeredTileBuilder: (int index) =>
                        new StaggeredTile.count(2, index.isEven ? 2.75 : 2.8),
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                  ),
                ),
              ),
            )
          ]),
        ),
      ),
    );
  }

  bottomSheet() {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) => Container(
        height: MediaQuery.of(context).size.height * 0.90,
        decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(5.0),
            topRight: const Radius.circular(5.0),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Account Details",
                style: TextStyle(
                    fontFamily: "AxiformaBold",
                    fontSize: Constants().fontSize20,
                    color: Constants().fontBlack),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                decoration: BoxDecoration(
                    color: Constants().greyColorAppBar,
                    borderRadius: BorderRadius.all(Radius.circular(5))),
                child: TextField(
                  decoration: InputDecoration(
                      hintText: "Search with Keyword",
                      hintStyle: TextStyle(
                          color: Constants().greyColorLabel,
                          fontFamily: "AxiformaRegular"),
                      prefixIcon: Icon(
                        Icons.search,
                        color: Color(
                          0xFF99999b,
                        ),
                      ),
                      //   filled: true,
                      // fillColor: Color(  ),
                      border: InputBorder.none),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: 3,
                      itemBuilder: (context, index) {
                        return ExpansionTile(
                            title: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Brand",
                                    style: TextStyle(
                                        fontFamily: "AxiformaBold",
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold,
                                        color: Constants().fontBlack),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(left: 0),
                                      child: dynamicChips(true))
                                ]),
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(left: 15),
                                child: dynamicChips(false),
                              ),
                            ]);
                      }),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  dynamicChips(bool isHead) {
    // List<String> selectedChoices = List();

    return Container(
        width: MediaQuery.of(context).size.width,
        child: MultiSelectChip(_dynamicChips, isHead));
  }
}
