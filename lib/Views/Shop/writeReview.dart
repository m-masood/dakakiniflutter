import 'package:app/Utills/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class WriteReviewScreen extends StatefulWidget {
  @override
  _WriteReviewScreenState createState() => _WriteReviewScreenState();
}

class _WriteReviewScreenState extends State<WriteReviewScreen> {
  List<String> rtnOptnList = new List();

  @override
  void initState() {
    // TODO: implement initState

    rtnOptnList.add("value");
    rtnOptnList.add("value");
    rtnOptnList.add("value");
    rtnOptnList.add("value");
    rtnOptnList.add("value");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0,
          backgroundColor: colorMain,
         titleSpacing: 0.0,
         centerTitle: false,
         title: Text("Write Review",
           style: TextStyle(
             color: Colors.white,
             fontFamily: "AxiformaBold",
             fontSize: Constants().fontSize18,
           ),
         ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () => Navigator.pop(context),
          ),
        actions:actionWidgets(context),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            children: [
             
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Column(children: [
                  Text(
                    "Anim cillum do cupidatat laboru ad. Anim cillum do cupidatat laborum pariatur exercitation ad.",
                    style: TextStyle(
                      fontFamily: "AxiformaRegular",
                      color: Constants().fontBlack,
                      height: 1.5,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 20),
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: rtnOptnList.length,
                    itemBuilder: (context, index) {
                      return ratingValues("Rating Metrics");
                    },
                  ),

                  Padding(
                    padding: EdgeInsets.only(bottom: 20),
                  ),
                  Container(
                    padding: const EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0.0, 3.0), //(x,y)
                            blurRadius: 10.0,
                          )
                        ]),
                    child: TextField(
                      minLines: 3,
                      maxLines: 10,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Write your comment here....",
                          hintStyle: TextStyle(
                              fontFamily: "AxiformaRegular,",
                              color: Constants().greyColorLabel,
                              fontSize: Constants().fontSize18)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 20),
                  ),
                  GestureDetector(
                    onTap: () {
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width/1.1,
                      height: btnHeight,
                      decoration: BoxDecoration(
                        color: colorMain,
                        borderRadius: BorderRadius.all(
                            Radius.circular(8)),
                      ),
                      child: Center(
                        child: Text(
                          "Done",
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: "AxiformaBold",
                              fontSize: Constants().fontSize15),
                        ),
                      ),
                    ),
                  ),

                ]),
              )
            ],
          ),
        ),
      ),
    );
  }

  ratingValues(String name) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(name,
            style: TextStyle(
                fontFamily: "AxiformaRegular",
                color: Constants().fontBlack,
                fontSize: Constants().fontSize15)),
        RatingBar(
          itemSize: 30,
          initialRating: 3,
          minRating: 1,
          direction: Axis.horizontal,
          allowHalfRating: false,
          itemCount: 5,
          itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: Colors.amber,
          ),
          onRatingUpdate: (rating) {
            print(rating);
          },
        ),
      ],
    );
  }
}
