import 'package:app/Utills/constants.dart';
import 'package:flutter/material.dart';

class ManualAddVet extends StatefulWidget {
  @override
  _ManualAddVetState createState() => _ManualAddVetState();
}

class _ManualAddVetState extends State<ManualAddVet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Constants().greyColorAppBar,
          leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: Constants().fontBlack,
              )),
          elevation: 0,
        ),
        body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 10, left: 20),
                width: MediaQuery.of(context).size.width,
                color: Constants().greyColorAppBar,
                child: Text(
                  "Add a Vet",
                  style: TextStyle(
                      fontFamily: "AxiformaBold",
                      color: Constants().blue,
                      fontSize: Constants().fontSize20 + 2),
                ),
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    tfANDheading("Vet Name", true, false, false, false),
                    tfANDheading("Vet Clinic Name", false, true, false, false),
                    tfANDheading("Vet Name", false, false, true, false),
                    tfANDheading("Clinic Address", false, false, false, true),
                    GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 30),
                        decoration: BoxDecoration(
                            color: Constants().fontOrange,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(0.0, 5.0), //(x,y)
                                blurRadius: 15.0,
                              )
                            ]),
                        child: Text("Done",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "AxiformaBold",
                                fontSize: Constants().fontSize15)),
                      ),
                    ),
                  ],
                ),
              )
            ])));
  }

  tfANDheading(
      String heading, bool isName, bool isClncNm, isPhn, bool isclncAddrss) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          heading,
          style: TextStyle(
              fontFamily: "AxiformaBold", color: Constants().fontBlack),
        ),
        Padding(padding: EdgeInsets.only(bottom: 5)),
        Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 15),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0.0, 5.0), //(x,y)
                    blurRadius: 15.0,
                  )
                ]),
            child: TextField(
              minLines: isclncAddrss == true ? 3 : 1,
              maxLines: null,
              keyboardType:
                  isPhn == true ? TextInputType.phone : TextInputType.text,
              decoration: InputDecoration(
                  hintText: isName == true
                      ? "John Smith"
                      : isClncNm == true
                          ? "John Smith's Clinic"
                          : isPhn == true ? "+1 (000) 000-0000" : "",
                  border: InputBorder.none),
            )),
        Padding(padding: EdgeInsets.only(bottom: 20)),
      ],
    );
  }
}
