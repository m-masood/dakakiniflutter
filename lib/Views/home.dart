import 'package:app/Models/MedicineModel.dart';
import 'package:app/Models/MyPetsModel.dart';
import 'package:app/Models/PetModel.dart';
import 'package:app/Utills/constants.dart';
import 'package:app/Views/AddPet.dart';
import 'package:app/Views/Shop/ProductListPage.dart';
import 'package:app/Views/Shop/ProductDetails.dart';
import 'package:app/Views/Shop/ShopProducts.dart';
import 'package:app/Views/Shop/ShopsView.dart';
import 'package:app/Views/SideBarMenu.dart/SideBarMenu.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<PetModel> widgetList = new List();
  List<MedicineModel> medicineList = new List();
  List<MyPets> myPets = new List();
  int _currentImageIndex = 0;
  int _current = 0;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    widgetList.add(new PetModel("Electronics", "https://www.online-tech-tips.com/wp-content/uploads/2019/12/electronic-gadgets.jpeg", Constants().greyColorAppBar));
    widgetList.add(new PetModel("Food", "https://heremag-prod-app-deps-s3heremagassets-bfie27mzpk03.s3.amazonaws.com/wp-content/uploads/2019/09/25175815/al-fanar-restaurant-dubai-2D8A0671-31-1600x1067.jpg", Constants().greyColorAppBar));
    widgetList.add(new PetModel("Sweets", "https://i.pinimg.com/originals/e6/02/31/e60231abf00b6b97707f4c9df4c553d7.jpg", Constants().greyColorAppBar));
    widgetList.add(new PetModel("Grocery", "https://i0.wp.com/www.ansi.ph/wp-content/uploads/2014/05/dairy.jpg?resize=1200%2C632&ssl=1", Constants().greyColorAppBar));
    widgetList.add(new PetModel("Shoes", "https://www.arabianbusiness.com/sites/default/files/article_embed_images/Jordan+store+1.jpg", Constants().greyColorAppBar));

    medicineList.add(new MedicineModel(
        "American Journey", "Assets/royalcaninLOGO.png", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/royalcaninLOGO.png", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/royalcaninLOGO.png", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/royalcaninLOGO.png", "Salmon & Sw...", 39.99, 4));

    myPets.add(new MyPets(
        "Jerry", "Assets/myDog.jpg", "Pomeranian", "Male", 3, 4.6, true));
    myPets.add(new MyPets(
        "Jerry", "Assets/myDog.jpg", "Pomeranian", "Male", 3, 4.6, true));

    // TODO: implement initState
    super.initState();
//    _firebaseMessaging.configure(
//      onMessage: (Map<String, dynamic> message) async {
//        print("onMessage: $message");
//        //_showItemDialog(message);
//      },
//      onLaunch: (Map<String, dynamic> message) async {
//        print("onLaunch: $message");
//        //_navigateToItemDetail(message);
//      },
//      onResume: (Map<String, dynamic> message) async {
//        print("onResume: $message");
//        //_navigateToItemDetail(message);
//      },
//    );
//    _firebaseMessaging.requestNotificationPermissions(
//        const IosNotificationSettings(
//            sound: true, badge: true, alert: true, provisional: true));
//    _firebaseMessaging.onIosSettingsRegistered
//        .listen((IosNotificationSettings settings) {
//      print("Settings registered: $settings");
//    });
//    _firebaseMessaging.getToken().then((String token) {
//      assert(token != null);
//      print(token);
//    });
//
//    Crashlytics.instance.log('baz');
//    //throw StateError('Uncaught error thrown by app.');
//    // Crashlytics.instance.crash();
//    try {
//      throw 'error_example';
//    } catch (e, s) {
//      Crashlytics.instance.recordError(e, s, context: 'as an example');
//    }
  }

  @override
  Widget build(BuildContext context) {
    /*24 is for notification bar on Android*/
    /*24 is for notification bar on Android*/
    return  Scaffold(
      backgroundColor: Constants().greyColorAppBar,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 10),
                  child: Text(
                    "Top Categories",
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: "AxiformaRegular",
                        fontWeight: FontWeight.bold,
                        fontSize: Constants().fontSize20),
                  ),
                ),
                Container(

                    width: MediaQuery.of(context).size.width,
                    height: 120,
                    child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: widgetList.length,
                        itemBuilder: (BuildContext cntxt, int index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ProductListPage()));
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                      child: ClipRRect(
                                          borderRadius: BorderRadius.circular(65.0),
                                          child: CachedNetworkImage(
                                              fit: BoxFit.cover,
                                              width: 65,
                                              height: 65,
                                              imageUrl: widgetList[index].image,
                                              placeholder: (context, url) =>
                                                  noImageAvailable(
                                                      height: 65.0, width: 65.0),
                                              errorWidget: (context, url, error) =>
                                                  noImageAvailable(
                                                      height: 65.0, width: 65.0)))),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 5),
                                  ),
                                  Text(
                                    widgetList[index].name,
                                    style: TextStyle(
                                        fontFamily: "AxiformaRegular",
                                        color: Color(0xFF464648)),
                                  )
                                ],
                              ),
                            ),
                          );
                        })),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 5),
                  child: Text(
                    "Shops For you",
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: "AxiformaRegular",
                        fontWeight: FontWeight.bold,
                        fontSize: Constants().fontSize20),
                  ),
                ),
                dashboardSliderWidget(),
                shopList()
              ],
            ),
          ),
        ),
      
    );
  }



  headingAndValue(String heading, String value) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          heading,
          style: TextStyle(
              color: Constants().fontBlack,
              fontFamily: "AxiformaRegular",
              fontSize: Constants().fontSize11),
        ),
        Text(
          value,
          style: TextStyle(
              color: Constants().fontBlack,
              fontFamily: "AxiformaRegular",
              fontSize: Constants().fontSize13,
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }
  shopList(){
    return Container(
      child: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemCount: 4,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProductListPage()));
              },
              child: Padding(
                padding: EdgeInsets.symmetric(
                    vertical: 10, horizontal: 10),
                child: Card(
                  elevation: 4.5,
                  child: Container(
                      padding: EdgeInsets.only(bottom:5),
                      width: MediaQuery.of(context).size.width - 25,
                      height: 280,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                          BorderRadius.all(Radius.circular(5)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              offset: Offset(0.0, 3.0), //(x,y)
                              blurRadius: 10.0,
                            )
                          ]),
                      child: Column(
                        children: <Widget>[
                          Container(
                              width: MediaQuery.of(context).size.width,
                              height: 160,
                              child :ClipRRect(
                                borderRadius: BorderRadius.only(topRight: Radius.circular(5),
                                    topLeft:Radius.circular(5) ),
                                child: CachedNetworkImage(
                                    height: 160.0,
                                    width: MediaQuery.of(context).size.width,
                                    fit: BoxFit.cover,
                                    imageUrl:'https://www.telegraph.co.uk/content/dam/Travel/hotels/middle-east/united-arab-emirates/dubai/dubai-shopping-guide-lead.jpg',
                                    placeholder: (context, url) =>
                                        dummyCoverImage(context, true),
                                    errorWidget: (context, url, error) =>
                                        dummyCoverImage(context, false)

                                ),
                              )
                          ),
                          Container(
                            color: Colors.white,
                            margin: EdgeInsets.zero,
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(0.0, 0.0, 2.0, 0.0),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                child: Flexible(
                                                  child: Text(
                                                    "My Shop Title",
                                                    style:TextStyle(
                                                        fontFamily: "AxiformaBold",
                                                        fontSize: Constants().fontSize13,
                                                        fontWeight: FontWeight.bold,
                                                        color: Constants().fontBlack),
                                                    overflow: TextOverflow.ellipsis,
                                                    maxLines: 1,
                                                    softWrap: false,
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Flexible(
                                                  child: Text(
                                                    dummytxt,
                                                    style:
                                                    TextStyle(
                                                        fontFamily: "AxiformaRegular",
                                                        fontSize: Constants().fontSize11,
                                                        color: Constants().fontBlack),
                                                    overflow: TextOverflow.ellipsis,
                                                    maxLines: 2,
                                                    softWrap: false,
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top: 0.0),
                                                child: Row(
                                                  children: <Widget>[
                                                    Container(
                                                      padding: EdgeInsets.zero,
                                                      decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(3.0),
                                                        color: colorMain,
                                                      ),
                                                      child: Padding(
                                                        padding: const EdgeInsets.all(3.0),
                                                        child: Text("Clothing & sweets",
                                                          softWrap: false,
                                                          overflow: TextOverflow.ellipsis,
                                                          maxLines: 1,
                                                          style: TextStyle(
                                                              fontFamily: "AxiformaRegular",
                                                              fontSize: Constants().fontSize11,
                                                              color: Colors.white),
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.only(left: 3.0),
                                                      child: Row(
                                                        children: <Widget>[
                                                          SvgPicture.asset(
                                                            "Assets/location_line.svg",
                                                            color: colorMain,
                                                            allowDrawingOutsideViewBox: true,
                                                            height: 15,
                                                          ),
                                                          Text(
                                                            'Open: 12:00 AM',
                                                            overflow: TextOverflow.ellipsis,
                                                            maxLines: 1,
                                                            softWrap: false,
                                                            style:TextStyle(
                                                                fontFamily: "AxiformaRegular",
                                                                fontSize: Constants().fontSize11,
                                                                color: Constants().fontBlack),
                                                          ),
                                                          Text(
                                                            ' Close: 12:00 PM',
                                                            overflow: TextOverflow.ellipsis,
                                                            maxLines: 1,
                                                            softWrap: false,
                                                            style: TextStyle(
                                                                fontFamily: "AxiformaRegular",
                                                                fontSize: Constants().fontSize11,
                                                                color: Constants().fontBlack),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top: 4.0),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    SvgPicture.asset(
                                                      "Assets/location_line.svg",
                                                      allowDrawingOutsideViewBox: true,
                                                      height: 15,
                                                      color: colorMain,
                                                    ),
                                                    Flexible(
                                                      child: Padding(
                                                        padding: const EdgeInsets.only(left: 4.0),
                                                        child: Text(
                                                          "Sheikh Mohammed Bin Rashed Boulevard,  Dubai,  9770,  Dubai,  United Arab Emirates",
                                                          overflow: TextOverflow.ellipsis,
                                                          maxLines: 1,
                                                          style:TextStyle(
                                                              fontFamily: "AxiformaRegular",
                                                              fontSize: Constants().fontSize11,
                                                              color: Constants().fontBlack),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      )),
                ),
              ),
            );
          }),
    );
  }
  dashboardSliderWidget() {
    return Container(
        margin: EdgeInsets.only(top: 5, bottom: 10),
        child: Column(
          children: <Widget>[
            CarouselSlider.builder(
              itemCount: 5,
              height: 190,
              enlargeCenterPage: true,
              viewportFraction: 0.9,
              aspectRatio: 2.0,
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 3),
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              autoPlayCurve: Curves.fastLinearToSlowEaseIn,
              onPageChanged: (index) {
                setState(() {
                  _current = index;
                });
              },
              itemBuilder: (context, index) {
                var bannerImage ='https://image.freepik.com/free-vector/abstract-mega-sales-background-flat-design_23-2148236283.jpg';
                return Container(
                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                  child: ClipRRect(
                      borderRadius:
                      BorderRadius.all(Radius.circular(7.0)),
                      child: Stack(children: <Widget>[
                        CachedNetworkImage(
                            fit: BoxFit.cover,
                            width: MediaQuery.of(context).size.width,
                            height: 1000,
                            imageUrl: bannerImage,
                            placeholder: (context, url) =>
                                dummyCoverImage(context, true),
                            errorWidget: (context, url, error) =>
                                dummyCoverImage(context, false)),
                      ])),
                );
              },
            ),
          ],
        ));
  }

}
