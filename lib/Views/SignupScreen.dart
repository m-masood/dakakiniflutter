import 'package:app/Utills/constants.dart';
import 'package:app/Views/MainBottomBar.dart';
import 'package:app/Views/home.dart';
import 'package:flutter/material.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorMain,
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: <Widget>[

              Padding(
                padding: const EdgeInsets.only(
                  top: 40.0,
                  left: 15,
                  right: 15,
                ),
                child: ListView(
                  children: <Widget>[
                    Text(
                      "Welcome to",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: "AxiformaBold",
                          fontSize: Constants().fontSize20 - 2),
                    ),
                    Text(
                      "Dakakini",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: "AxiformaBold",
                          fontSize: Constants().fontSize32),
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 20)),
                    Container(
                      height: MediaQuery.of(context).size.height,
                      padding: EdgeInsets.only(top: 15, right: 15, left: 15),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10))),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,

                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "SIGN",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: "AxiformaBold",
                                          fontSize: Constants().fontSize25),
                                    ),
                                    Text(
                                      "UP",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: "AxiformaBold",
                                          decoration: TextDecoration.underline,
                                          decorationColor: colorMain,
                                          fontSize: Constants().fontSize25),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: editTxtHeight,
                                  child: Card(
                                      elevation: 20,
                                      child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: TextField(
                                              decoration: new InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText:
                                                      'Enter Your Full Name',
                                                  labelText: 'Full Name',
                                                  labelStyle: TextStyle(

                                                      color: Constants()
                                                          .greyColorLabel))))),
                                ),
                                Padding(padding: EdgeInsets.only(bottom: 10)),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: editTxtHeight,
                                  child: Card(
                                      elevation: 20,
                                      child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: TextField(
                                              decoration: new InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText:
                                                      'Enter Your Email',
                                                  labelText: 'Email Address',
                                                  labelStyle: TextStyle(
                                                      color: Constants()
                                                          .greyColorLabel))))),
                                ),
                                Padding(padding: EdgeInsets.only(bottom: 10)),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: editTxtHeight,
                                  child: Card(
                                      elevation: 20,
                                      child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: TextField(
                                              decoration: new InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: 'Enter Password',
                                                  labelText: 'Password',
                                                  labelStyle: TextStyle(
                                                      color: Constants()
                                                          .greyColorLabel))))),
                                ),
                                Padding(padding: EdgeInsets.only(bottom: 10)),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: editTxtHeight,
                                  child: Card(
                                      elevation: 20,
                                      child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: TextField(
                                              decoration: new InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: 'Enter Password',
                                                  labelText: 'Confirm Password',
                                                  labelStyle: TextStyle(
                                                      color: Constants()
                                                          .greyColorLabel))))),
                                ),
                                Padding(padding: EdgeInsets.only(bottom: 10)),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: editTxtHeight,
                                  child: Card(
                                      elevation: 20,
                                      child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: TextField(
                                              decoration: new InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: 'Enter Mobile Number',
                                                  labelText: 'Mobile Number',
                                                  labelStyle: TextStyle(
                                                      color: Constants()
                                                          .greyColorLabel))))),
                                ),
                                Padding(padding: EdgeInsets.only(bottom: 10)),

                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: editTxtHeight,
                                  child: Card(
                                      elevation: 20,
                                      child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: TextField(
                                              decoration: new InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: 'Enter Address',
                                                  labelText: 'Address',
                                                  labelStyle: TextStyle(
                                                      color: Constants()
                                                          .greyColorLabel))))),
                                ),
                                Padding(padding: EdgeInsets.only(bottom: 15)),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                MyHomePage()));
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width/1.1,
                                    height: btnHeight,
                                    decoration: BoxDecoration(
                                      color: colorMain,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8)),
                                    ),
                                    child: Center(
                                      child: Text(
                                        "Create Account",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: "AxiformaBold",
                                            fontSize: Constants().fontSize15),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
