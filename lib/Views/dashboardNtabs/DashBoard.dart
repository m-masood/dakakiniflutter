import 'package:app/Utills/constants.dart';
import 'package:app/Views/dashboardNtabs/AllTab.dart';
import 'package:app/Views/dashboardNtabs/Autoship.dart';
import 'package:app/Views/dashboardNtabs/OrdersTab.dart';
import 'package:app/Views/dashboardNtabs/PrescriptionList.dart';
import 'package:app/Views/dashboardNtabs/SettingsTab.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 5, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      appBar: AppBar(
//        brightness: Brightness.light,
//        backgroundColor: Constants().greyColorAppBar,
//        elevation: 0,
//        title: Text(
//          "Dashboard",
//          style: TextStyle(
//            color: Constants().appbarTextclr,
//            fontFamily: "AxiformaBold",
//          ),
//        ),
//      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                Constants().gradientStart,
                Constants().gradientEnd,
              ])),
              child: TabBar(
                isScrollable: true,
                unselectedLabelColor: Constants().tabBarUNFOCUSclr,
                labelColor: Colors.white,
                tabs: [
                  new Tab(
                    text: "All",
                  ),
                  new Tab(
                    text: "Orders",
                  ),
                  new Tab(
                    text: "Autoship",
                  ),
                    new Tab(
                    text: "Prescription",
                  ),
                  new Tab(
                    text: "Settings",
                  ),
                ],
                controller: _tabController,
                indicatorColor: Color(0xFFff866a),
                indicatorWeight: 4.0,
                indicatorSize: TabBarIndicatorSize.tab,
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                child: TabBarView(
                  children: [AllTab(), MyOrders(), Autoship(),Prescription(), SettingsTab()],
                  controller: _tabController,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
