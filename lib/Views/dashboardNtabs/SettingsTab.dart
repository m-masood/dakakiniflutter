import 'package:app/Utills/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SettingsTab extends StatefulWidget {
  @override
  _SettingsTabState createState() => _SettingsTabState();
}

class _SettingsTabState extends State<SettingsTab> {
  List<String> shippingAddrss = List();
  List<String> paymentAddrss = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // shippingAddrss.add("A");
    // shippingAddrss.add("A");
    // shippingAddrss.add("A");

    // paymentAddrss.add("A");
    // paymentAddrss.add("A");
    // paymentAddrss.add("A");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(bottom: 10),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        "Account Details",
                        style: TextStyle(
                            fontFamily: "AxiformaBold",
                            fontSize: Constants().fontSize18,
                            color: Constants().blue),
                      ),
                    ),
                    Icon(Icons.edit)
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 10),
                ),
                headingValue("Name", "Muhammad Muzammil"),
                headingValue("Email", "Muhammad Muzammil"),
                headingValue("Password", "************"),
                Padding(
                  padding: EdgeInsets.only(bottom: 10),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        "Shipping Address",
                        style: TextStyle(
                            fontFamily: "AxiformaBold",
                            fontSize: Constants().fontSize18,
                            color: Constants().blue),
                      ),
                    ),
                    Text("Add",
                        style: TextStyle(
                            fontFamily: "AxiformaRegular",
                            fontSize: Constants().fontSize18,
                            color: Constants().fontBlack,
                            decoration: TextDecoration.underline)),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 10),
                ),
                shippingAddrss.isEmpty
                    ? listEmptyTile("There are no shipping addresses to show")
                    : ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: shippingAddrss.length,
                        itemBuilder: (context, index) {
                          return shippingAddressCard(
                              "Cecilia Chapman",
                              "711-2880 Nulla St.",
                              "Mankato Mississippi 96522",
                              "(257) 563-7401");
                        }),
                Padding(
                  padding: EdgeInsets.only(bottom: 10),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        "Payment Methods",
                        style: TextStyle(
                            fontFamily: "AxiformaBold",
                            fontSize: Constants().fontSize18,
                            color: Constants().blue),
                      ),
                    ),
                    Text("Add",
                        style: TextStyle(
                            fontFamily: "AxiformaRegular",
                            fontSize: Constants().fontSize18,
                            color: Constants().fontBlack,
                            decoration: TextDecoration.underline)),
                  ],
                ),
                paymentAddrss.isEmpty?
                listEmptyTile("There are no payment methods to show")
                :ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: paymentAddrss.length,
                    itemBuilder: (context, index) {
                      return paymentMethodCard(
                          "Cecilia Chapman", "****  ****  ****  1232");
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }

  listEmptyTile(String value) {
    return Card(
      elevation: 4.5,
      child: Center(
        child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 15),
            child: Text(value,
                style: TextStyle(
                    fontFamily: "AxiformaRegular",
                    fontSize: Constants().fontSize15,
                    fontWeight: FontWeight.w600,
                    color: Constants().fontBlack))),
      ),
    );
  }

//for light heading and dark value
  headingValue(String heading, String value) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: <Widget>[
          Text(
            heading + ":  ",
            style: TextStyle(
              fontFamily: "AxiformaRegular",
              color: Constants().headingsLight,
              fontSize: Constants().fontSize15,
            ),
          ),
          Text(
            value,
            style: TextStyle(
              fontFamily: "AxiformaRegular",
              color: Constants().fontBlack,
              fontSize: Constants().fontSize15,
            ),
          ),
        ],
      ),
    );
  }

  shippingAddressCard(
    String address,
    String city,
    String state,
    String phoneNumber,
  ) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Card(
        elevation: 4.5,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    address,
                    style: TextStyle(
                        fontFamily: "AxiformaBold",
                        color: Constants().fontBlack),
                  ),
                  Icon(Icons.edit)
                ],
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 3),
              ),
              Text(city,
                  style: TextStyle(
                      color: Constants().headingsLight,
                      fontFamily: "AxiformRegular")),
              Text(state,
                  style: TextStyle(
                      color: Constants().headingsLight,
                      fontFamily: "AxiformRegular")),
              Text(phoneNumber,
                  style: TextStyle(
                      color: Constants().headingsLight,
                      fontFamily: "AxiformRegular")),
            ],
          ),
        ),
      ),
    );
  }

  paymentMethodCard(String name, String cardNumber) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Card(
        elevation: 4.5,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    name,
                    style: TextStyle(
                        fontFamily: "AxiformaBold",
                        color: Constants().fontBlack),
                  ),
                  Icon(Icons.edit)
                ],
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 3),
              ),
              Text(cardNumber,
                  style: TextStyle(
                      color: Constants().headingsLight,
                      fontFamily: "AxiformRegular")),
            ],
          ),
        ),
      ),
    );
  }
}
