import 'package:app/Utills/constants.dart';
import 'package:flutter/material.dart';

class PrescriptionDetails extends StatefulWidget {
  @override
  _PrescriptionDetailsState createState() => _PrescriptionDetailsState();
}

class _PrescriptionDetailsState extends State<PrescriptionDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Constants().greyColorAppBar,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Constants().fontBlack,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Center(
                  child: Text(
                "Valid",
                style: TextStyle(fontFamily: "AxiformaRegular", height: 1),
              )),
              decoration: BoxDecoration(
                  color: Constants().green,
                  borderRadius: BorderRadius.all(Radius.circular(20))),
            ),
          )
        ],
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: Constants().greyColorAppBar,
              padding: EdgeInsets.only(left: 20, bottom: 10),
              width: MediaQuery.of(context).size.width,
              child: Text(
                "Rx# 2132141",
                style: TextStyle(
                    color: Constants().appbarTextclr,
                    fontFamily: "AxiformaBold",
                    fontSize: Constants().fontSize25,
                    height: 1),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 20),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Item Information",
                        style: TextStyle(
                            color: Constants().appbarTextclr,
                            fontFamily: "AxiformaRegular",
                            fontSize: Constants().fontSize20,
                            height: 1),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                      ),
                      itemInfo("Assets/galliprant.png",
                          "American Journey Salmon & Sw...", "\$39.99", "1"),
                      Padding(
                        padding: EdgeInsets.only(bottom: 20),
                      ),
                      Text(
                        "Pet Information",
                        style: TextStyle(
                            color: Constants().appbarTextclr,
                            fontFamily: "AxiformaRegular",
                            fontSize: Constants().fontSize20,
                            height: 1),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                      ),
                      petInformation("Assets/dog.png", "Jerry", "Pomeranian",
                          "Male-3 Kgs", 4.6, true),
                      Padding(
                        padding: EdgeInsets.only(bottom: 20),
                      ),
                      Text(
                        "Vet Information",
                        style: TextStyle(
                            color: Constants().appbarTextclr,
                            fontFamily: "AxiformaRegular",
                            fontSize: Constants().fontSize20,
                            height: 1),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                      ),
                      vetInfo("STRAY PETS IN NEED OF MA INC 781-235-1218",
                          "5 Overbrook TerNatick, MA 01760"),
                      Padding(
                        padding: EdgeInsets.only(bottom: 20),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 10),
                              decoration: BoxDecoration(
                                  color: Constants().greyColorLabel,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              child: Center(
                                child: Text(
                                  "View PDF",
                                  style: TextStyle(
                                    fontFamily: "AxiformaBold",
                                    color: Colors.white,
                                    fontSize: Constants().fontSize18,
                                    height: 1,
                                  ),
                                ),
                              )),
                          Padding(padding: EdgeInsets.only(left: 20)),
                          Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 10),
                              decoration: BoxDecoration(
                                  color: Constants().fontOrange,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              child: Center(
                                child: Text(
                                  "Reorder",
                                  style: TextStyle(
                                    fontFamily: "AxiformaBold",
                                    color: Colors.white,
                                    fontSize: Constants().fontSize18,
                                    height: 1,
                                  ),
                                ),
                              )),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                      ),
                      
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  vetInfo(clinicName, clinicAddress) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
          color: Constants().greyColorAppBar,
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          headingAndValue1(
              "Vet Clinic Name", "STRAY PETS IN NEED OF MA INC781-235-1218"),
          headingAndValue1("Address", "5 Overbrook Ter Natick, MA 01760"),
        ],
      ),
    );
  }

  itemInfo(String image, String name, String cost, String quantity) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      decoration: BoxDecoration(
          color: Constants().greyColorAppBar,
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            image,
            height: MediaQuery.of(context).size.height / 8,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 2,
                child: Text(
                  name,
                  style: TextStyle(
                    fontFamily: "AxiformaBold",
                    height: 1,
                    color: Constants().fontBlack,
                    fontSize: Constants().fontSize15,
                  ),
                  maxLines: 2,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.55,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          "\$39.",
                          style: TextStyle(
                              height: 0.75,
                              fontFamily: "AxiformaBold",
                              fontWeight: FontWeight.bold,
                              fontSize: Constants().fontSize20,
                              color: Constants().fontOrange),
                        ),
                        Text(
                          "99",
                          style: TextStyle(
                              height: 1,
                              fontFamily: "AxiformaBold",
                              fontWeight: FontWeight.bold,
                              fontSize: Constants().fontSize15,
                              color: Constants().fontOrange),
                        )
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Image.asset(
                          "Assets/rxLOGOtp.png",
                          height: MediaQuery.of(context).size.height / 25,
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 5),
                        ),
                        Text(
                          "Quantity: $quantity",
                          style: TextStyle(
                              fontFamily: "AxiformaRegular", height: 1),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  headingAndValue1(String heading, String value) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width / 2.5,
          child: Text(
            heading,
            style: TextStyle(
                color: Constants().fontBlack,
                fontFamily: "AxiformaRegular",
                fontSize: Constants().fontSize11),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width / 2.5,
          child: Text(
            value,
            style: TextStyle(
                color: Constants().fontBlack,
                fontFamily: "AxiformaRegular",
                fontSize: Constants().fontSize13,
                fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }

  headingAndValue(String heading, String value) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          heading,
          style: TextStyle(
              color: Constants().fontBlack,
              fontFamily: "AxiformaRegular",
              fontSize: Constants().fontSize11),
        ),
        Text(
          value,
          style: TextStyle(
              color: Constants().fontBlack,
              fontFamily: "AxiformaRegular",
              fontSize: Constants().fontSize13 - 1,
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }

  petInformation(String image, String name, String breed, String gender,
      double age, bool isMedication) {
    return Container(
        decoration: BoxDecoration(
            color: Constants().greyColorAppBar,
            borderRadius: BorderRadius.all(Radius.circular(5))),
        padding: EdgeInsets.all(5),
        child: Row(
          children: <Widget>[
            Container(
                width: 75,
                height: 90,
                decoration: new BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(
                          image,
                        )))),
            Padding(
              padding: EdgeInsets.only(right: 10),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  name,
                  style: TextStyle(
                      color: Constants().blue,
                      fontFamily: "PoppinsBold",
                      fontSize: Constants().fontSize20),
                ),
                Text(breed,
                    style: TextStyle(
                        color: Constants().fontBlack,
                        fontFamily: "AxiformaRegular",
                        fontSize: Constants().fontSize13)),
                Row(
                  children: <Widget>[
                    headingAndValue("Gender", gender),
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                    ),
                    headingAndValue("Age", age.toString() + " Years"),
                    Padding(
                      padding: EdgeInsets.only(left: 15),
                    ),
                    headingAndValue("Medications", isMedication ? "Yes" : "No"),
                  ],
                )
              ],
            )
          ],
        ));
  }
}
