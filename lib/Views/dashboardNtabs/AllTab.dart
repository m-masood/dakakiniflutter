import 'package:app/Models/MedicineModel.dart';
import 'package:app/Utills/constants.dart';
import 'package:app/Views/AddPet.dart';
import 'package:app/Views/AddVet.dart';
import 'package:app/Views/Shop/ProductDetails.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class AllTab extends StatefulWidget {
  @override
  _AllTabState createState() => _AllTabState();
}

class _AllTabState extends State<AllTab> {
  List<MedicineModel> medicineList = new List();

  List<String> autoshipList = new List();
  List<String> prescriptionList = new List();

  List<String> petList = new List();
  List<String> vetList = new List();

  @override
  void initState() {
    prescriptionList.add("A");
    prescriptionList.add("A");
    prescriptionList.add("A");

    autoshipList.add("A");
    autoshipList.add("A");
    autoshipList.add("A");

    petList.add("A");
    petList.add("A");
    petList.add("A");

    vetList.add("A");
    vetList.add("A");
    vetList.add("A");

    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
            top: 20,
          ),
          width: MediaQuery.of(context).size.width,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                  Widget>[
            Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15.0),
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                        width: MediaQuery.of(context).size.width,
                        // height: 200,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            // gradient: LinearGradient(colors: [
                            //   Constants().gradientEnd,
                            //   Constants().gradientStart
                            // ]),
                            image: DecorationImage(
                                image: AssetImage("Assets/newsBG.png"),
                                fit: BoxFit.fitHeight)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("Covid-19 Notification",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "AxiformaBold")),
                            Container(
                              width: MediaQuery.of(context).size.width / 2.3,
                              child: Text(
                                "Mollit elit magna magna dolor id nostrud. Sunt ea nostrud sint aliqua. Nostrud minim laborum dolor fugiat cillum aliqua incididunt in id id.",
                                style: TextStyle(
                                  color: Color(0xFFc0bdd8),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 20),
                            ),
                            Text(
                              "Dismiss",
                              style: TextStyle(
                                  color: Constants().dismisTxtClr,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 15,
                                  decoration: TextDecoration.underline),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Align(
                    alignment: Alignment.topRight,
                    child: Image.asset(
                      "Assets/avatar.png",
                      height: 200,
                      width: 200,
                    )),
              ],
            ),
            Padding(padding: EdgeInsets.only(bottom: 20)),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "Autoship Items",
                      style: TextStyle(
                          fontFamily: "AxiformaBold",
                          fontSize: Constants().fontSize18,
                          color: Constants().blue),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => AddPet()));
                    },
                    child: Text("Add",
                        style: TextStyle(
                            fontFamily: "AxiformaRegular",
                            fontSize: Constants().fontSize18,
                            color: Constants().fontBlack,
                            decoration: TextDecoration.underline)),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5.0),
              child: Container(
                height: 135,
                child: autoshipList.isEmpty
                    ? emptyListFunc("Add Autoships", "Assets/addPet.png")
                    : ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: petList.length,
                        itemBuilder: (context, index) {
                          return autoshipListTile(
                              "American Journey",
                              "Salmon & Sw...",
                              "Assets/galliprant.png",
                              1,
                              "Every 2 Weeks",
                              "Next Shipment in 5 days",
                              "Active");
                        }),
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 10)),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "Your Prescriptions ",
                      style: TextStyle(
                          fontFamily: "AxiformaBold",
                          fontSize: Constants().fontSize18,
                          color: Constants().blue),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => AddPet()));
                    },
                    child: Text("Add",
                        style: TextStyle(
                            fontFamily: "AxiformaRegular",
                            fontSize: Constants().fontSize18,
                            color: Constants().fontBlack,
                            decoration: TextDecoration.underline)),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5.0),
              child: Container(
                height: 135,
                child: prescriptionList.isEmpty
                    ? emptyListFunc("Add Prescription", "Assets/addPet.png")
                    : ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: prescriptionList.length,
                        itemBuilder: (context, index) {
                          return prescriptionListTile(
                              "American Journey",
                              "Salmon & Sw...",
                              "Assets/galliprant.png",
                              1,
                              "Rx# 51465498456",
                              "July 15, 21",
                              "valid");
                        }),
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 10)),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "Your Pets",
                      style: TextStyle(
                          fontFamily: "AxiformaBold",
                          fontSize: Constants().fontSize18,
                          color: Constants().blue),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => AddPet()));
                    },
                    child: Text("Add",
                        style: TextStyle(
                            fontFamily: "AxiformaRegular",
                            fontSize: Constants().fontSize18,
                            color: Constants().fontBlack,
                            decoration: TextDecoration.underline)),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5.0),
              child: Container(
                height: 137,
                child: petList.isEmpty
                    ? emptyListFunc("Add Your Pets", "Assets/addPet.png")
                    : ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: petList.length,
                        itemBuilder: (context, index) {
                          return yourPetWidget("Jerry", "Pomeranian",
                              "Male - 3 Kgs", "4 Years", "Yes");
                        }),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 10, horizontal: 20.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "Your Vets",
                      style: TextStyle(
                          fontFamily: "AxiformaBold",
                          fontSize: Constants().fontSize18,
                          color: Constants().blue),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => AddVet()));
                    },
                    child: Text("Add",
                        style: TextStyle(
                            fontFamily: "AxiformaRegular",
                            fontSize: Constants().fontSize18,
                            color: Constants().fontBlack,
                            decoration: TextDecoration.underline)),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5.0),
              child: Container(
                height: vetList.isEmpty ? 135 : 128,
                child: vetList.isEmpty
                    ? emptyListFunc("Add Your Vet", "Assets/addVet.png")
                    : ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: 2,
                        itemBuilder: (context, index) {
                          return yourVets(
                            "STRAY PETS IN NEED OF MA INC 781-235-1218",
                            "5 Overbrook Ter Natick, MA 01760",
                          );
                        }),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 10, horizontal: 20.0),
              child: Text(
                "People Also Bought",
                style: TextStyle(
                    fontFamily: "AxiformaBold",
                    fontSize: Constants().fontSize18,
                    color: Constants().blue),
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width,
                height: 250,
                child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: medicineList.length,
                    itemBuilder: (BuildContext cntxt, int index) {
                      List<String> price =
                          medicineList[index].price.toString().split(".");
                      String priceBeforeDec = price[0];
                      String priceAfterDec = price[1];
                      return productsList(
                          medicineList[index].image,
                          medicineList[index].name,
                          medicineList[index].brand,
                          medicineList[index].ratings,
                          priceBeforeDec,
                          priceAfterDec);
                    }))
          ]),
        ),
      ),
    );
  }

  emptyListFunc(String title, String image) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 10.0,
      ),
      child: Container(
          padding: EdgeInsets.only(left: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.white),
                    ),
                    padding: EdgeInsets.all(5),
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                  ),
                  Text(title,
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: "AxiformaRegular",
                          fontSize: Constants().fontSize18)),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Image.asset(
                  image,
                  fit: BoxFit.fitHeight,
                  height: MediaQuery.of(context).size.height / 5.5,
                ),
              )
            ],
          ),
          decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey,
                  blurRadius: 10.0,
                  offset: Offset(0.0, 0.5))
            ],
            gradient: LinearGradient(colors: [
              Constants().gradientStart,
              Constants().gradientEnd,
            ]),
            borderRadius: BorderRadius.all(Radius.circular(5)),
          )),
    );
  }

  vetHeadingValueFunc(String key, String value) {
    return Container(
      width: MediaQuery.of(context).size.width / 4,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            key,
            style: TextStyle(
                color: Constants().headingsLight,
                fontFamily: "AxiformRegular",
                fontWeight: FontWeight.w400,
                fontSize: Constants().fontSize11 + 1),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 5),
          ),
          Text(
            value,
            style: TextStyle(
                color: Constants().fontBlack,
                fontFamily: "AxiformBold",
                fontWeight: FontWeight.w500,
                fontSize: Constants().fontSize13),
          )
        ],
      ),
    );
  }

  headingValueFunc(String key, String value) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          key,
          style: TextStyle(
              color: Constants().headingsLight,
              fontFamily: "AxiformRegular",
              fontWeight: FontWeight.w400,
              fontSize: Constants().fontSize11 + 1),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 5),
        ),
        Text(
          value,
          style: TextStyle(
              color: Constants().fontBlack,
              fontFamily: "AxiformBold",
              fontWeight: FontWeight.w500,
              fontSize: Constants().fontSize13),
        )
      ],
    );
  }

  yourPetWidget(String name, String bread, String gender, String age,
      String meditations) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 10.0,
      ),
      child: Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Container(
            padding: EdgeInsets.all(10),
            width: MediaQuery.of(context).size.width - 30,
            decoration: BoxDecoration(
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    offset: Offset(0.0, 0.5))
              ],
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: 80.0,
                  height: 95.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          'Assets/myDog.jpg',
                        )),
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: Colors.redAccent,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 5),
                ),
                Expanded(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              name,
                              style: TextStyle(
                                  fontFamily: "AxiformaBold",
                                  fontSize: Constants().fontSize15,
                                  color: Constants().blue),
                            ),
                            Icon(
                              Icons.edit,
                              size: 20,
                            )
                          ],
                        ),
                        Text(
                          bread,
                          style: TextStyle(
                              fontFamily: "AxiformaRegular",
                              fontSize: Constants().fontSize13,
                              color: Constants().headingsLight),
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 10)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            headingValueFunc("Gender", gender),
                            headingValueFunc("Age", age),
                            headingValueFunc("Medications", meditations),
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          )),
    );
  }

  yourVets(String clinicName, String address) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 10.0,
      ),
      child: Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Container(
            padding: EdgeInsets.all(10),
            width: MediaQuery.of(context).size.width - 30,
            decoration: BoxDecoration(
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    offset: Offset(0.0, 0.5))
              ],
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5)),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                vetHeadingValueFunc("Vet Clinic Name", clinicName),
                vetHeadingValueFunc("Address", address),
                Icon(
                  Icons.edit,
                  size: 20,
                )
              ],
            ),
          )),
    );
  }

  prescriptionListTile(String name, String company, String img, int qntity,
      String prescriptionNumber, String expiry, String status) {
    return Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 10.0,
          vertical: 10,
        ),
        child: Container(
            width: MediaQuery.of(context).size.width - 30,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(5),
              ),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    offset: Offset(0.0, 0.5))
              ],
            ),
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 70.0,
                  height: 85.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          img,
                        )),
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(prescriptionNumber,
                                  style: TextStyle(
                                      color: Constants().greyColorLabel,
                                      fontFamily: "AxiformaRegular",
                                      fontSize: Constants().fontSize13)),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 3, horizontal: 10),
                                decoration: BoxDecoration(
                                    color: Constants().green,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                                child: Text(status,
                                    style: TextStyle(
                                      color: Colors.white,
                                    )),
                              )
                            ],
                          ),
                          Text(
                            name,
                            style: TextStyle(
                                fontFamily: "AxiformaBold",
                                color: Constants().fontBlack,
                                fontSize: Constants().fontSize13 + 1,
                                height: 1),
                          ),
                          Text(
                            company,
                            style: TextStyle(
                                fontFamily: "AxiformaRegular",
                                color: Constants().fontBlack,
                                fontSize: Constants().fontSize13 + 1,
                                height: 1),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Quantity: $qntity",
                              style: TextStyle(
                                  fontFamily: "AxiformaRegular",
                                  fontWeight: FontWeight.w500,
                                  color: Constants().fontBlack,
                                  fontSize: Constants().fontSize13)),
                          Text(
                            "Expiry Date: $expiry",
                            style: TextStyle(
                                fontFamily: "AxiformaRegular",
                                fontSize: Constants().fontSize11,
                                color: Constants().greyColorLabel),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            )));
  }

  autoshipListTile(String name, String company, String img, int qntity,
      String schedule, String nxtShipment, String status) {
    return Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 10.0,
          vertical: 10,
        ),
        child: Container(
            width: MediaQuery.of(context).size.width - 30,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(5),
              ),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    offset: Offset(0.0, 0.5))
              ],
            ),
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 70.0,
                  height: 85.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          img,
                        )),
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                name,
                                style: TextStyle(
                                    fontFamily: "AxiformaBold",
                                    color: Constants().fontBlack,
                                    fontSize: Constants().fontSize13 + 1,
                                    height: 1),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 3, horizontal: 10),
                                decoration: BoxDecoration(
                                    color: Constants().green,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                                child: Text("Valid",
                                    style: TextStyle(
                                      color: Colors.white,
                                    )),
                              )
                            ],
                          ),
                          Text(
                            company,
                            style: TextStyle(
                                fontFamily: "AxiformaRegular",
                                color: Constants().fontBlack,
                                fontSize: Constants().fontSize13 + 1,
                                height: 1),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Quantity: $qntity",
                              style: TextStyle(
                                  fontFamily: "AxiformaRegular",
                                  fontWeight: FontWeight.w500,
                                  color: Constants().fontBlack,
                                  fontSize: Constants().fontSize13)),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                "$schedule",
                                style: TextStyle(
                                    fontFamily: "AxiformaRegular",
                                    fontSize: Constants().fontSize13,
                                    color: Constants().fontBlack,
                                    fontWeight: FontWeight.w600),
                              ),
                              Text(
                                "$nxtShipment",
                                style: TextStyle(
                                    fontFamily: "AxiformaRegular",
                                    fontSize: Constants().fontSize13,
                                    color: Constants().greyColorLabel,
                                    height: 1),
                              ),
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            )));
  }

  productsList(String image, String name, String brand, double rating,
      String priceBeforeDec, String priceAfterDec) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ProductDetail()));
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          padding: const EdgeInsets.all(10.0),
          height: 250,
          width: 160,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 3.0), //(x,y)
                  blurRadius: 10.0,
                )
              ]),
          child: Column(
            children: <Widget>[
              Image.asset(
                image,
                width: 100,
                height: 100,
              ),
              Padding(
                padding: EdgeInsets.only(top: 5),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  name,
                  style: TextStyle(
                      fontFamily: "AxiformaBold",
                      fontSize: Constants().fontSize13,
                      fontWeight: FontWeight.bold,
                      color: Constants().fontBlack),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  brand,
                  style: TextStyle(
                      fontFamily: "AxiformaRegular",
                      fontSize: Constants().fontSize11,
                      color: Constants().fontBlack),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: RatingBarIndicator(
                  rating: rating,
                  itemBuilder: (context, index) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  itemCount: 5,
                  itemSize: 15.0,
                  direction: Axis.horizontal,
                ),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Row(
                      children: <Widget>[
                        Text(
                          priceBeforeDec,
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              color: Constants().fontOrange,
                              fontSize: Constants().fontSize25),
                        ),
                        Text(
                          "." + priceAfterDec,
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              color: Constants().fontOrange,
                              fontSize: Constants().fontSize15),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        "Free 1-2 days",
                        style: TextStyle(
                            fontFamily: "AxiformaRegular",
                            color: Constants().fontBlack,
                            fontWeight: FontWeight.bold,
                            fontSize: Constants().fontSize11 - 1),
                      ),
                      Text("Shipping",
                          style: TextStyle(
                              fontFamily: "AxiformaRegular",
                              color: Constants().fontBlack,
                              fontWeight: FontWeight.bold,
                              fontSize: Constants().fontSize11 - 1))
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
