import 'package:app/Utills/constants.dart';
import 'package:app/Views/dashboardNtabs/PrescriptionDetails.dart';
import 'package:flutter/material.dart';

class Prescription extends StatefulWidget {
  @override
  _PrescriptionState createState() => _PrescriptionState();
}

class _PrescriptionState extends State<Prescription> {
  List<String> prescriptionList = new List();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    prescriptionList.add("A");
    prescriptionList.add("A");
    prescriptionList.add("A");
    prescriptionList.add("A");
    prescriptionList.add("A");
    prescriptionList.add("A");
    prescriptionList.add("A");
    prescriptionList.add("A");
    prescriptionList.add("A");
    prescriptionList.add("A");
    prescriptionList.add("A");
    prescriptionList.add("A");
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: prescriptionList.length,
          itemBuilder: (context, index) {
            return GestureDetector(
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PrescriptionDetails())),
                child: prescriptionListTile(
                    "American Journey",
                    "Salmon & Sw...",
                    "Assets/galliprant.png",
                    1,
                    "Rx# 51465498456",
                    "July 15, 21",
                    "valid"));
          }),
    );
  }

  prescriptionListTile(String name, String company, String img, int qntity,
      String prescriptionNumber, String expiry, String status) {
    return Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 10.0,
          vertical: 10,
        ),
        child: Container(
            width: MediaQuery.of(context).size.width - 30,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(5),
              ),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    offset: Offset(0.0, 0.5))
              ],
            ),
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 70.0,
                  height: 85.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(
                          img,
                        )),
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(prescriptionNumber,
                                  style: TextStyle(
                                      color: Constants().greyColorLabel,
                                      fontFamily: "AxiformaRegular",
                                      fontSize: Constants().fontSize13)),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 3, horizontal: 10),
                                decoration: BoxDecoration(
                                    color: Constants().green,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                                child: Text(status,
                                    style: TextStyle(
                                      color: Colors.white,
                                    )),
                              )
                            ],
                          ),
                          Text(
                            name,
                            style: TextStyle(
                                fontFamily: "AxiformaBold",
                                color: Constants().fontBlack,
                                fontSize: Constants().fontSize13 + 1,
                                height: 1),
                          ),
                          Text(
                            company,
                            style: TextStyle(
                                fontFamily: "AxiformaRegular",
                                color: Constants().fontBlack,
                                fontSize: Constants().fontSize13 + 1,
                                height: 1),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 20),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Quantity: $qntity",
                              style: TextStyle(
                                  fontFamily: "AxiformaRegular",
                                  fontWeight: FontWeight.w500,
                                  color: Constants().fontBlack,
                                  fontSize: Constants().fontSize13)),
                          Text(
                            "Expiry Date: $expiry",
                            style: TextStyle(
                                fontFamily: "AxiformaRegular",
                                fontSize: Constants().fontSize11,
                                color: Constants().greyColorLabel),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            )));
  }
}
