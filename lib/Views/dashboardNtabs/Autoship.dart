import 'package:app/Models/AutopshipModel.dart';
import 'package:app/Utills/constants.dart';
import 'package:flutter/material.dart';

class Autoship extends StatefulWidget {
  @override
  _AutoshipState createState() => _AutoshipState();
}

class _AutoshipState extends State<Autoship> {
  List<AutoshipModel> autoshipList = new List();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    autoshipList.add(new AutoshipModel("a", "b", "c", 'd'));
    autoshipList.add(new AutoshipModel("a", "b", "c", 'd'));
    autoshipList.add(new AutoshipModel("a", "b", "c", 'd'));
  }

  @override
  Widget build(BuildContext context) {
    print(autoshipList.isEmpty);
    return SingleChildScrollView(
      child: Container(
        child: Column(
          mainAxisAlignment: autoshipList.isEmpty
              ? MainAxisAlignment.center
              : MainAxisAlignment.start,
          children: <Widget>[
            autoshipList.isEmpty
                ? emptyAuthoShip()
                : ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: autoshipList.length,
                    itemBuilder: (context, index) {
                      return autoshipCard();
                    },
                  ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
              decoration: BoxDecoration(
                color: Constants().fontOrange,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Text(
                autoshipList.isEmpty ? "Order Now" : "Save and Update",
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: "AxiformaBold",
                    fontSize: Constants().fontSize13),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 20),
            ),
          ],
        ),
      ),
    );
  }

  emptyAuthoShip() {
    return Container(
        height: MediaQuery.of(context).size.height / 1.8,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Image.asset(
              "Assets/asEMPTY.png",
              height: MediaQuery.of(context).size.height / 3,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            ),
            Text(
              "You don’t have any\nAutoship items",
              style: TextStyle(
                  fontFamily: "AxiformaBold",
                  height: 1.2,
                  fontSize: Constants().fontSize20,
                  color: Constants().greyColorLabel),
              textAlign: TextAlign.center,
            )
          ],
        ));
  }

  autoshipCard() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Container(
        //height: 100,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 5.0), //(x,y)
            blurRadius: 15.0,
          )
        ]),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Image.asset(
                      "Assets/galliprant.png",
                      height: 50,
                    ),
                    //Padding(padding: EdgeInsets.only(left: 10),),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "American Journey",
                          style: TextStyle(fontFamily: "AxiformaBold"),
                        ),
                        Text(
                          "Salman & SW..",
                          style: TextStyle(fontFamily: "AxiformaRegular"),
                        ),
                      ],
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 2, horizontal: 7),
                      decoration: BoxDecoration(
                          color: Constants().green,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: Text(
                        "Active",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: Constants().fontSize11),
                      ),
                    ),
                    Text(
                      "Next Shipment ",
                      style: TextStyle(
                          fontFamily: "AxiformaRegular",
                          fontSize: Constants().fontSize13),
                    ),
                    Text(
                      "in 5 days ",
                      style: TextStyle(
                          fontFamily: "AxiformaBold",
                          fontSize: Constants().fontSize13),
                    ),
                  ],
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            ),
            greyContainer(Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text("Quantity: "),
                    Text(
                      "1",
                      style: TextStyle(fontFamily: "AxiformaBold"),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      "Assets/arrowUP.png",
                      height: 10,
                    ),
                    Image.asset(
                      "Assets/arrowDOWN.png",
                      height: 10,
                    ),
                  ],
                )
              ],
            )),
            greyContainer(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Frequency",
                  style: TextStyle(
                      fontFamily: "AxiformaRegular",
                      fontSize: Constants().fontSize11,
                      color: Constants().greyColorLabel),
                ),
                Container(
                  height: 22,
                  width: MediaQuery.of(context).size.width,
                  child: DropdownButtonHideUnderline(
                    child: new DropdownButton<String>(
                      hint: Text("Select Frequency"),
                      items: <String>[
                        'Every 1 Weeks',
                        'Every 2 Weeks',
                        'Every 3 Weeks',
                        'Every 4 Weeks'
                      ].map((String value) {
                        return new DropdownMenuItem<String>(
                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                      onChanged: (_) {},
                    ),
                  ),
                ),
              ],
            )),
            greyContainer(
              DropdownButtonHideUnderline(
                child: new DropdownButton<String>(
                  hint: Text("Select Pet"),
                  items: <String>[
                    'Dog',
                    'Cat',
                    'Parrot',
                  ].map((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList(),
                  onChanged: (_) {},
                ),
              ),
            ),
            greyContainer(
              DropdownButtonHideUnderline(
                child: new DropdownButton<String>(
                  hint: Text("Select Vet"),
                  items: <String>[
                    'Vet 1',
                    'Vet 2',
                    'Vet 3',
                  ].map((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList(),
                  onChanged: (value) {
                    print(value);
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  greyContainer(
    Widget widget,
  ) {
    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        height: 61,
        decoration: BoxDecoration(color: Constants().cardLgrey),
        child: widget,
      ),
    );
  }
}
