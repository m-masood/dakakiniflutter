import 'package:app/Utills/constants.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants().greyColorAppBar,
       appBar: customeAppbar("My Cart", context),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 5,
            child: Container(
              padding: EdgeInsets.only(left: 5,right: 5,top:5),
              //    height: 250,
              child: MyListView(5),
            ),
          ),

          //.........................5th Last Container
          Expanded(
            flex: 2,
            child: Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: upperRoundedCorners),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  //.........................7.a  Price Text Row................

                  Padding(
                    padding: const EdgeInsets.only(
                        left: 28.0, right: 28.0, bottom: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Sub Total",
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              fontSize: Constants().fontSize13,
                              color: colorMain),
                        ),
                        Text(
                          "2000 AED",
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              fontSize: Constants().fontSize13,
                              color: Colors.black),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 28.0, right: 28.0, bottom: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "VAT",
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              fontSize: Constants().fontSize13,
                              color: colorMain),
                        ),
                        Text(
                          "100 AED",
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              fontSize: Constants().fontSize13,
                              color: Colors.black),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 28.0, right: 28.0, bottom: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Shipping Fee",
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              fontSize: Constants().fontSize13,
                              color: colorMain),
                        ),
                        Text(
                          "50 AED",
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              fontSize: Constants().fontSize13,
                              color: Colors.black),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 28.0, right: 28.0, bottom: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Total",
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              fontSize: Constants().fontSize15,
                              color: colorMain),
                        ),
                        Text(
                          "2150 AED",
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              fontSize: Constants().fontSize15,
                              color: Colors.black),
                        )
                      ],
                    ),
                  ),

                  //...............................7.c CheckOut Button......................................
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 12.0, right: 12.0, bottom: 10.0),
                    child: ButtonTheme(
                      height: 45,
                      minWidth: MediaQuery.of(context).size.width,
                      buttonColor: colorMain,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(7)),
                      child: RaisedButton(
                        onPressed: () {
                         // Navigator.pushNamed(context, 'Delivery');
                        },
                        child: Text(
                          "CHECKOUT",
                          style: TextStyle(
                              fontFamily: "AxiformaBold",
                              fontSize: Constants().fontSize18,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  )
                ],
              ),
            ),
          )
        ],
      ),


    );

  }
  ListView MyListView(int newcount) {
    return ListView.builder(
        itemCount: 5,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            elevation: 4.5,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              //color: Colors.yellow,
              height: 100,
              child: Row(
                children: <Widget>[
                  // .........................Image.......................
                  ClipOval(
                    child: CachedNetworkImage(
                      imageUrl: 'https://image.freepik.com/free-vector/abstract-mega-sales-background-flat-design_23-2148236283.jpg',
                      height: 80,
                      width: 80,
                      fit: BoxFit.cover,
                    ),
                  ),

                  SizedBox(
                    width: 20,
                  ),
                  //.........................Text............................
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 18.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text("Name"),
                            SizedBox(
                              width: 10,
                            ),
                            Text("300",
                              style:
                              TextStyle(fontSize: 12, color: Colors.grey),
                            )
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 10.0),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(20)),
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.delete),
                              SizedBox(
                                width: 20,
                              ),
                              Text('1'),
                              SizedBox(
                                width: 20,
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    //count = count + 1;
                                  });
                                },
                                child: Icon(
                                  Icons.add,
                                  color: Colors.orangeAccent,
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Spacer(),

                  //.......................Cancel Icon........................
                  IconButton(
                    icon: Icon(
                      Icons.cancel,
                      color: colorMain,
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }


  emptyCart(){
    return  Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 50),
        ),
        Row(
          children: [
            Padding(padding: EdgeInsets.only(left: 50),),
            Image.asset(
              "Assets/cartEmpty.png",
              height: MediaQuery.of(context).size.height / 4,
            ),
          ],
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 10),
        ),
        Text(
          "You don’t have any\nAutoship items",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontFamily: "AxiformaBold",
              height: 1.2,
              fontSize: Constants().fontSize20,
              color: Constants().greyColorLabel),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 50),
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
          decoration: BoxDecoration(
            color: Constants().fontOrange,
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Text(
            "Order Now",
            style: TextStyle(
                color: Colors.white,
                fontFamily: "AxiformaBold",
                fontSize: Constants().fontSize13),
          ),
        ),
      ],
    );
  }
}
