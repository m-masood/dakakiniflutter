import 'package:app/Utills/constants.dart';
import 'package:app/Views/CartScreen.dart';
import 'package:app/Views/Components/custom_drawer.dart';
import 'package:app/Views/LoginSreen.dart';
import 'package:app/Views/dashboardNtabs/DashBoard.dart';
import 'package:app/Views/home.dart';
import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Widget> screens = [HomeScreen(), CartScreen(),Dashboard(),];
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  int currentIndex;
  @override
  void initState() {
    super.initState();
    currentIndex = 0;
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
    key: _scaffoldKey,
        drawer: CustomDrawer(),
        appBar: AppBar(
          backgroundColor: colorMain,
          elevation: 0.0,
          centerTitle: true,
          title: Text('Dakakini',style: TextStyle(
            color: Colors.white,
            fontFamily: "AxiformaBold",
            fontSize: Constants().fontSize18,
          ),),
          leading:   GestureDetector(
            onTap: (){
              _scaffoldKey.currentState.openDrawer();
            },
            child: Icon(
              Icons.menu,
              color: Colors.white,
              size: 28,
            ),
          ),
          actions: actionWidgets(context),
        ),
//        bottomNavigationBar: BubbleBottomBar(
//          iconSize: 20,
//          opacity: .4,
//          currentIndex: currentIndex,
//          onTap: changePage,
//          elevation: 9,
//          items: <BubbleBottomBarItem>[
//            BubbleBottomBarItem(
//                backgroundColor: Colors.deepPurple,
//                icon: Image.asset(
//                  "Assets/homeUFCS.png",
//                  height: 20,
//                  width: 20,
//                ),
//                activeIcon: Image.asset(
//                  "Assets/homeFCS.png",
//                  height: 20,
//                  width: 20,
//                ),
//                title: Text("Home")),
//            BubbleBottomBarItem(
//                backgroundColor: Colors.indigo,
//                icon: Image.asset(
//                  "Assets/cartUFCS.png",
//                  height: 20,
//                  width: 20,
//                ),
//                activeIcon: Image.asset(
//                  "Assets/cartFCS.png",
//                  height: 20,
//                  width: 20,
//                ),
//                title: Text("Cart")),
//            BubbleBottomBarItem(
//                backgroundColor: Constants().gradientStart,
//                icon: Image.asset(
//                  "Assets/accountUFCS.png",
//                  height: 20,
//                  width: 20,
//                ),
//                activeIcon: Image.asset(
//                  "Assets/accountFCS.png",
//                  height: 20,
//                  width: 20,
//                ),
//                title: Text("Account")),
//
//
//          ],
//        ),
        body: screens[currentIndex]);
  }

}


