import 'package:app/Utills/constants.dart';
import 'package:flutter/material.dart';

class AllQuestions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            elevation: 0,
            backgroundColor: Constants().greyColorAppBar,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Constants().fontBlack,
              ),
              onPressed: () => Navigator.pop(context),
            )),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                color: Constants().greyColorAppBar,
                padding: EdgeInsets.only(left: 20, bottom: 10),
                width: MediaQuery.of(context).size.width,
                child: Text(
                  "All Questions",
                  style: TextStyle(
                      color: Constants().appbarTextclr,
                      fontFamily: "AxiformaBold",
                      fontSize: Constants().fontSize25,
                      height: 1),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 0),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Constants().greyColorAppBar,
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  child: TextField(
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        prefixIcon: Icon(
                          Icons.search,
                          color: Constants().greyColorLabel,
                        ),
                        hintText: "Search with Keyword",
                        hintStyle: TextStyle(
                            fontFamily: "AxiformaRegular",
                            color: Constants().greyColorLabel)),
                  ),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  //  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: 5,
                  itemBuilder: (context, index) {
                    return questionTile(
                        "Where is this food made ?",
                        "The American Journey Grain-free line is produced in Kansas while the new American Journey Limited Ingredient Grain-Free line is produced in California",
                        "29 June 2020",
                        "By PharmaZu",
                        13,
                        context);
                  },
                ),
              )
            ],
          ),
        ));
  }

  questionTile(String qstn, String answr, String date, String name, int liked,
      BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        padding: const EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                offset: Offset(0.0, 3.0), //(x,y)
                blurRadius: 10.0,
              )
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    date,
                    style: TextStyle(
                        fontFamily: "AxiformaRegular",
                        color: Constants().greyColorLabel,
                        fontSize: Constants().fontSize13),
                  ),
                  Text(
                    "Answer this question",
                    style: TextStyle(
                        fontFamily: "AxiformaRegular",
                        color: Constants().fontOrange,
                        fontSize: Constants().fontSize13),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            ),
            Text(
              qstn,
              style: TextStyle(
                  color: Constants().appbarTextclr,
                  fontFamily: "AxiformaBold",
                  fontSize: Constants().fontSize18,
                  height: 1),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            ),
            Text(
              answr,
              style: TextStyle(fontFamily: "AxiformaRegular"),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  name,
                  style: TextStyle(
                      fontFamily: "AxiformaRegular",
                      color: Constants().greyColorLabel,
                      fontSize: Constants().fontSize13,
                      height: 1),
                ),
                Padding(padding: EdgeInsets.only(left: 10)),
                Image.asset(
                  "Assets/thumbsUp.png",
                  height: MediaQuery.of(context).size.height / 30,
                ),
                Padding(padding: EdgeInsets.only(left: 5)),
                Text(
                  "$liked people marked Helpful",
                  style: TextStyle(
                      fontFamily: "AxiformaRegular",
                      color: Constants().greyColorLabel,
                      fontSize: Constants().fontSize13,
                      height: 1,
                      decoration: TextDecoration.underline),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
