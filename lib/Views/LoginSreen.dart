import 'package:app/Utills/constants.dart';
import 'package:app/Views/MainBottomBar.dart';
import 'package:app/Views/SignupScreen.dart';
import 'package:app/Views/dashboardNtabs/DashBoard.dart';
import 'package:app/Views/home.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:colorMain,
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: <Widget>[

              Padding(
                padding: const EdgeInsets.only(
                  top: 40,
                  left: 15,
                  right: 15,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Welcome to",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: "AxiformaBold",
                            fontSize: Constants().fontSize20 - 2),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:8.0,right: 8.0),
                      child: Text(
                        "Dakakini",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: "AxiformaBold",
                            fontSize: Constants().fontSize32),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 70)),
                    Expanded(
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.only(top: 15, right: 15, left: 15),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10))),
                        child: Column(

                          children: <Widget>[

                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        "LOG",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: "AxiformaBold",
                                            fontSize: Constants().fontSize25),
                                      ),
                                      Text(
                                        "IN",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: "AxiformaBold",
                                            decoration: TextDecoration.underline,
                                            decorationColor: colorMain,
                                            fontSize: Constants().fontSize25),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 40,),
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: editTxtHeight,
                                    child: Card(
                                        elevation: 20,
                                        child: Padding(
                                            padding: const EdgeInsets.all(5.0),
                                            child: TextField(
                                                decoration: new InputDecoration(
                                                    border: InputBorder.none,
                                                    hintText:
                                                        'Enter Your Email',
                                                    labelText: 'Email Address',
                                                    labelStyle: TextStyle(
                                                        color: Constants()
                                                            .greyColorLabel))))),
                                  ),
                                  Padding(padding: EdgeInsets.only(bottom: 10)),
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: editTxtHeight,
                                    child: Card(
                                        elevation: 20,
                                        child: Padding(
                                            padding: const EdgeInsets.all(5.0),
                                            child: TextField(
                                                decoration: new InputDecoration(
                                                    border: InputBorder.none,
                                                    hintText: 'Enter Password',
                                                    labelText: 'Password',
                                                    labelStyle: TextStyle(
                                                        color: Constants()
                                                            .greyColorLabel))))),
                                  ),
                                  Padding(padding: EdgeInsets.only(bottom: 10)),
                                  Align(
                                    alignment: Alignment.bottomRight,
                                    child: Text(
                                      "Forget Password?",
                                      style: TextStyle(
                                          fontFamily: "AxiformaRegular",
                                          color: Constants().fontBlack),
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.only(bottom: 15)),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  MyHomePage()));
                                    },
                                    child: Container(
                                      width: MediaQuery.of(context).size.width/1.1,
                                      height: btnHeight,
                                      decoration: BoxDecoration(
                                        color: colorMain,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8)),
                                      ),
                                      child: Center(
                                        child: Text(
                                          "Login",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: "AxiformaBold",
                                              fontSize: Constants().fontSize15),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.only(bottom: 15)),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        "If you don’t have an account?",
                                        style: TextStyle(
                                          fontFamily: "AxiformaBold",
                                          fontSize: Constants().fontSize15,
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      SignupScreen()));
                                        },
                                        child: Text(
                                          "Sign up",
                                          style: TextStyle(
                                              fontFamily: "AxiformaBold",
                                              fontSize: Constants().fontSize15,
                                              decoration: TextDecoration.underline),
                                        ),
                                      )
                                    ],
                                  ),

                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
