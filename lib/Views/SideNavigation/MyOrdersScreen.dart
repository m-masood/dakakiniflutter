import 'package:app/Models/MedicineModel.dart';
import 'package:app/Utills/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class MyOrderScreen extends StatefulWidget {
  @override
  _MyOrderScreenState createState() => _MyOrderScreenState();
}

class _MyOrderScreenState extends State<MyOrderScreen> {
  List<MedicineModel> medicineList = new List();
  List<String> ordersList = new List();

  @override
  void initState() {
    // TODO: implement initState
    ordersList.add("A");
    ordersList.add("A");
    ordersList.add("A");

    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    medicineList.add(new MedicineModel(
        "American Journey", "Assets/apoque.jpeg", "Salmon & Sw...", 39.99, 4));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customeAppbar("My Orders", context),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.only(top: 5),
        color: Color(0xFFfefefe),
        child: ordersList.isEmpty
            ? emptyOrderList()
            : ListView.builder(
                shrinkWrap: true,
                itemCount: ordersList.length,
                itemBuilder: (context, index) {
                  return myOrderWidget(medicineList);
                }),
      ),
    );
  }

  myOrderWidget(
    List<MedicineModel> list,
  ) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Container(
          padding: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey, blurRadius: 10.0, offset: Offset(0.0, 0.5))
          ], color: Colors.white),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Order#",
                        style: TextStyle(
                            color: Color(0xFFa3a3a3),
                            fontFamily: "AxiformaRegular",
                            fontSize: Constants().fontSize13),
                      ),
                      Text(
                        "1325324132321",
                        style: TextStyle(
                            color: Constants().fontBlack,
                            fontFamily: "AxiformaBold",
                            fontSize: Constants().fontSize13),
                      ),
                      Text(
                        "Placed on 24 March 2020",
                        style: TextStyle(
                            color: Color(0xFFa3a3a3),
                            fontFamily: "AxiformaRegular",
                            fontSize: Constants().fontSize13),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        decoration: BoxDecoration(
                          color: Constants().green,
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                        ),
                        child: Text(
                          "Delivered",
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: "AxiformaRegular",
                              fontSize: Constants().fontSize11),
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        bottom: 6,
                      )),
                      Text(
                        "USD 89.99",
                        style: TextStyle(
                            color: Constants().fontOrange,
                            fontFamily: "AxiformaBold",
                            fontSize: Constants().fontSize18),
                      )
                    ],
                  )
                ],
              ),
              Container(
                height: 245,
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: list.length,
                  itemBuilder: (context, index) {
                    return MyOrderScreenCardView(
                        list[index].image,
                        list[index].name,
                        list[index].brand,
                        list[index].price,
                        list[index].ratings);
                  },
                ),
              ),
            ],
          )),
    );
  }

  emptyOrderList() {
    return Container(
        height: MediaQuery.of(context).size.height / 1.8,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "Assets/asEMPTY.png",
              height: MediaQuery.of(context).size.height / 3,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            ),
            Text(
              "You don’t have any\nAutoship items",
              style: TextStyle(
                  fontFamily: "AxiformaBold",
                  height: 1.2,
                  fontSize: Constants().fontSize20,
                  color: Constants().greyColorLabel),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
              decoration: BoxDecoration(
                color: Constants().fontOrange,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Text(
                "Order Now",
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: "AxiformaBold",
                    fontSize: Constants().fontSize13),
              ),
            ),
          ],
        ));
  }

  MyOrderScreenCardView(
      String image, String name, String brand, double prices, double ratings) {
    List<String> price = prices.toString().split(".");
    String priceBeforeDecimel = price[0];
    String priceAfterDecimel = price[1];

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        padding: const EdgeInsets.all(10.0),
        height: 300,
        width: 160,
        decoration: BoxDecoration(
          color: Constants().cardLgrey,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey, blurRadius: 1.0, offset: Offset(0.0, 0.5))
          ],
        ),
        child: Column(
          children: <Widget>[
            Image.asset(
              image,
              width: 100,
              height: 100,
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                name,
                style: TextStyle(
                    fontFamily: "AxiformaBold",
                    fontSize: Constants().fontSize13,
                    fontWeight: FontWeight.bold,
                    color: Constants().fontBlack),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                brand,
                style: TextStyle(
                    fontFamily: "AxiformaRegular",
                    fontSize: Constants().fontSize11,
                    color: Constants().fontBlack),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: RatingBarIndicator(
                rating: ratings,
                itemBuilder: (context, index) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                itemCount: 5,
                itemSize: 15.0,
                direction: Axis.horizontal,
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Text(
                        priceBeforeDecimel,
                        style: TextStyle(
                            fontFamily: "AxiformaBold",
                            color: Constants().fontOrange,
                            fontSize: Constants().fontSize25),
                      ),
                      Text(
                        "." + priceAfterDecimel,
                        style: TextStyle(
                            fontFamily: "AxiformaBold",
                            color: Constants().fontOrange,
                            fontSize: Constants().fontSize15),
                      ),
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "Free 1-2 days",
                      style: TextStyle(
                          fontFamily: "AxiformaRegular",
                          color: Constants().fontBlack,
                          fontWeight: FontWeight.bold,
                          fontSize: Constants().fontSize11 - 1),
                    ),
                    Text("Shipping",
                        style: TextStyle(
                            fontFamily: "AxiformaRegular",
                            color: Constants().fontBlack,
                            fontWeight: FontWeight.bold,
                            fontSize: Constants().fontSize11 - 1))
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
