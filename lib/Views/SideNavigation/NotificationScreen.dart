import 'package:app/Utills/constants.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class NotificationsScreen extends StatefulWidget {

  @override
  _NotificationsState createState() => _NotificationsState();
}
//
class _NotificationsState extends State<NotificationsScreen> {




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customeAppbar("Notification",context),
      body: Container(
        padding: EdgeInsets.only(top: 10),
        child: notificationsList(),
      ),
    );
  }


  notificationsList() {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: 12,
        itemBuilder: (ctx, int) {
          var profileUrl = 'https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg';
          var userName = 'Samer Ali';
          var companyName = 'Mobx pvt';
          var notificationType = 'New product';
          var textNoti ='Lorem added new product in his shop';
          return Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 2.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(50.0),
                              child: CachedNetworkImage(
                                  fit: BoxFit.cover,
                                  width: 50,
                                  height: 50,
                                  imageUrl: profileUrl,
                                  placeholder: (context, url) =>
                                      noImageAvailableForUser(
                                          height: 50.0, width: 50.0),
                                  errorWidget: (context, url, error) =>
                                      noImageAvailableForUser(
                                          height: 50.0, width: 50.0)))),
                      Padding(padding: EdgeInsets.only(left: 10.0)),
                      Expanded(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "$textNoti",
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 13.0),
                              ),
                              Text(
                                '14 minutes ago',
                               // "${computeTimeAndDate(notification.createdAt)}",
                                maxLines:1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: Colors.blue, fontSize: 13.0),
                              ),
                            ],
                          ))
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  Divider(
                    color: colorDivider,
                    height: 2,
                  )
                ],
              ));
        });
  }



}
