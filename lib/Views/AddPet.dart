import 'package:app/Utills/constants.dart';
import 'package:flutter/material.dart';

class AddPet extends StatefulWidget {
  @override
  _AddPetState createState() => _AddPetState();
}

class _AddPetState extends State<AddPet> {
  int _radioValue1 = 0;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          break;
        case 1:
          break;
        case 2:
          break;
      }
    });
  }

  List<String> petType = ["Dog", 'Cat', "Sheep"];
  String _selectedpetType;

  List<String> currentMedications = ["a", "b", "c"];
  String _SelectedCurrentMedication;
  String _SelectedAllergies;
  String _SelectedPreExisting;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants().greyColorAppBar,
        elevation: 0,
        leading: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(
              Icons.arrow_back,
              color: Constants().fontBlack,
            )),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 10,
                  color: Constants().greyColorAppBar,
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 20),
                    child: Text(
                      "Add a Pet",
                      style: TextStyle(
                          color: Constants().blue,
                          fontFamily: "AxiformaBold",
                          fontSize: Constants().fontSize20 + 2),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Stack(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 05.0, bottom: 10, right: 10),
                          child: Container(
                            height: 120,
                            padding: EdgeInsets.all(25),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(0.0, 3.0), //(x,y)
                                    blurRadius: 10.0,
                                  )
                                ]),
                            child: Image.asset(
                              "Assets/addPetAvatar.png",
                              height: 50,
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 90.0, right: 7),
                        child: Align(
                            alignment: Alignment.bottomRight,
                            child: Image.asset(
                              "Assets/bluePlus.png",
                              height: 25,
                            )),
                      )
                    ],
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Text(
                "Pet Detials",
                style: TextStyle(
                    color: Constants().blue,
                    fontFamily: "AxiformaBold",
                    fontSize: Constants().fontSize20),
              ),
            ),
            Expanded(
                          child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20, horizontal: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Pet Name",
                                style: TextStyle(
                                    fontFamily: "AxiformaBold",
                                    color: Constants().fontBlack),
                              ),
                              Padding(padding: EdgeInsets.only(bottom: 5)),
                              Container(
                                  width: MediaQuery.of(context).size.width / 2.2,
                                  padding: EdgeInsets.symmetric(horizontal: 15),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey,
                                          offset: Offset(0.0, 5.0), //(x,y)
                                          blurRadius: 15.0,
                                        )
                                      ]),
                                  child: TextField(
                                    decoration: InputDecoration(
                                        hintText: "Enter Name",
                                        border: InputBorder.none),
                                  ))
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Pet Type",
                                style: TextStyle(
                                    fontFamily: "AxiformaBold",
                                    color: Constants().fontBlack),
                              ),
                              Padding(padding: EdgeInsets.only(bottom: 5)),
                              Container(
                                width: MediaQuery.of(context).size.width / 2.2,
                                padding: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey,
                                        offset: Offset(0.0, 5.0), //(x,y)
                                        blurRadius: 15.0,
                                      )
                                    ]),
                                child: dropDownMenu(
                                    petType, context, true, false, false, false),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Pet Bread",
                                style: TextStyle(
                                    fontFamily: "AxiformaBold",
                                    color: Constants().fontBlack),
                              ),
                              Padding(padding: EdgeInsets.only(bottom: 5)),
                              Container(
                                  width: MediaQuery.of(context).size.width / 2.2,
                                  padding: EdgeInsets.symmetric(horizontal: 15),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey,
                                          offset: Offset(0.0, 5.0), //(x,y)
                                          blurRadius: 15.0,
                                        )
                                      ]),
                                  child: TextField(
                                    decoration: InputDecoration(
                                        hintText: "Enter Bread",
                                        border: InputBorder.none),
                                  ))
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Pet Type",
                                style: TextStyle(
                                    fontFamily: "AxiformaBold",
                                    color: Constants().fontBlack),
                              ),
                              Padding(padding: EdgeInsets.only(bottom: 5)),
                              Container(
                                width: MediaQuery.of(context).size.width / 2.2,
                                padding: EdgeInsets.symmetric(horizontal: 15),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey,
                                        offset: Offset(0.0, 5.0), //(x,y)
                                        blurRadius: 15.0,
                                      )
                                    ]),
                                child: dropDownMenu(
                                    petType, context, true, false, false, false),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 20),
                            child: Text(
                              "Gender",
                              style: TextStyle(
                                  fontFamily: "AxiformaBold",
                                  color: Constants().fontBlack),
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              new Radio(
                                value: 0,
                                groupValue: _radioValue1,
                                onChanged: _handleRadioValueChange1,
                                activeColor: Constants().radioBTnClr,
                              ),
                              new Text(
                                'Male',
                                style: new TextStyle(fontSize: 16.0),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 20),
                              ),
                              new Radio(
                                value: 1,
                                groupValue: _radioValue1,
                                onChanged: _handleRadioValueChange1,
                                activeColor: Constants().radioBTnClr,
                              ),
                              new Text(
                                'Female',
                                style: new TextStyle(fontSize: 16.0),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    // Padding(
                    //   padding: EdgeInsets.only(
                    //     bottom: 20,
                    //   ),
                    // ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Text(
                        "Weight",
                        style: TextStyle(
                            fontFamily: "AxiformaBold",
                            color: Constants().fontBlack),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 10.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                offset: Offset(0.0, 5.0), //(x,y)
                                blurRadius: 15.0,
                              )
                            ]),
                        child: TextField(
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Enter Weight",
                              suffixText: "lbs"),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      child: Text(
                        "Medication",
                        style: TextStyle(
                            color: Constants().blue,
                            fontFamily: "AxiformaBold",
                            fontSize: Constants().fontSize20),
                      ),
                    ),
                    dropDownContainer(
                        "Current Medications", currentMedications, "currentMeds"),
                    dropDownContainer(
                        "Allergies", currentMedications, "Allergies"),

                    dropDownContainer("Pre-Existing Conditions",
                        currentMedications, "preExisting"),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10),
                    ),

                    Center(
                      child: Container(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          width: MediaQuery.of(context).size.width / 3.5,
                          child: Center(
                              child: Text("Done",
                                  style: TextStyle(
                                    fontFamily: "AxiformaBold",
                                    color: Colors.white,
                                    fontSize: Constants().fontSize15,
                                  ))),
                          decoration: BoxDecoration(
                              color: Constants().fontOrange,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)))),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  dropDownContainer(String heading, List<String> list, String type) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Text(
            heading,
            style: TextStyle(
                fontFamily: "AxiformaBold", color: Constants().fontBlack),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 15),
            decoration: BoxDecoration(
                color: Colors.white,  
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0.0, 5.0), //(x,y)
                    blurRadius: 15.0,
                  )
                ]),
            child: type == "currentMeds"
                ? dropDownMenu(list, context, false, true, false, false)
                : type == "Allergies"
                    ? dropDownMenu(list, context, false, false, true, false)
                    : dropDownMenu(list, context, false, false, false, true),
          ),
        ),
      ],
    );
  }

  dropDownMenu(List<String> list, BuildContext context, bool isPetType,
      bool isMedication, bool isAllergies, bool isPreExisting) {
    return DropdownButtonHideUnderline(
      child: DropdownButton(
        // underline: SizedBox(),

        hint: Text(isPetType == true ? 'Select' : " Select Medications",
            style: TextStyle(fontSize: 15)), // Not necessary for Option 1
        value: isPetType == true
            ? _selectedpetType
            : isMedication == true
                ? _SelectedCurrentMedication
                : isAllergies == true
                    ? _SelectedAllergies
                    : _SelectedPreExisting,
        onChanged: (newValue) {
          setState(() {
            if (isPetType) {
              _selectedpetType = newValue;
            }
            if (isMedication) {
              _SelectedCurrentMedication = newValue;
            }
            if (isAllergies) {
              _SelectedAllergies = newValue;
            }
            if (isPreExisting) {
              _SelectedPreExisting = newValue;
            }
          });
        },
        items: list.map((location) {
          return DropdownMenuItem(
            child: new Text(location),
            value: location,
          );
        }).toList(),
      ),
    );
  }
}
