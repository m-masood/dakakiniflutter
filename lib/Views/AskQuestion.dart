import 'package:app/Utills/constants.dart';
import 'package:flutter/material.dart';

class AskQuestion extends StatefulWidget {
  @override
  _AskQuestionState createState() => _AskQuestionState();
}

class _AskQuestionState extends State<AskQuestion> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants().greyColorAppBar,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Constants().fontBlack,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                color: Constants().greyColorAppBar,
                padding: EdgeInsets.only(left: 20, bottom: 10),
                width: MediaQuery.of(context).size.width,
                child: Text(
                  "Ask a Question",
                  style: TextStyle(
                      color: Constants().appbarTextclr,
                      fontFamily: "AxiformaBold",
                      fontSize: Constants().fontSize25,
                      height: 1),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  children: [
                    Text(
                      "Anim cillum do cupidatat laboru ad. Anim cillum do cupidatat laborum pariatur exercitation ad.",
                      style: TextStyle(
                          fontFamily: "AxiformaRegular",
                          color: Constants().fontBlack,
                          fontWeight: FontWeight.w600),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[300],
                              offset: Offset(0.0, 3.0), //(x,y)
                              blurRadius: 10.0,
                            )
                          ]),
                      child: TextField(
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Write your question here....",
                            hintStyle: TextStyle(
                                color: Constants().greyColorLabel,
                                fontFamily: "AxiformaRegular")),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                    ),
                    SingleChildScrollView(
                      child: ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: 6,
                          itemBuilder: (context, index) {
                            return questionExpandable(
                                "Anim cillum do cupidatat laborum pariatur exercitation ad.",
                                "Anim cillum do cupidatat laborum pariatur exercitation ad.");
                          }),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width/3,
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        decoration: BoxDecoration(
                            color: Constants().fontOrange,
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: Center(
                          child: Text(
                            "Reorder",
                            style: TextStyle(
                              fontFamily: "AxiformaBold",
                              color: Colors.white,
                              fontSize: Constants().fontSize18,
                              height: 1,
                            ),
                          ),
                        )),
                        Padding(
                      padding: EdgeInsets.only(bottom: 20),
                    ),
                    Text("Got my answer",style: TextStyle(fontFamily: "AxiformaRegular",color: Constants().fontBlack),),
                      Padding(
                      padding: EdgeInsets.only(bottom: 5),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  questionExpandable(String question, String answer) {
    return ExpansionTile(
        title: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(
                question,
                style: TextStyle(
                    fontFamily: "AxiformaRegular",
                    fontSize: Constants().fontSize15,
                    fontWeight: FontWeight.bold,
                    color: Constants().fontBlack),
              ),
              Divider()
            ]),
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              answer,
              style: TextStyle(
                  fontFamily: "AxiformaRegular",
                  fontSize: Constants().fontSize15,
                  fontWeight: FontWeight.bold,
                  color: Constants().fontBlack),
            ),
          ),
        ]);
  }
}
