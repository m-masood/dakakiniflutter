import 'package:app/Utills/constants.dart';
import 'package:app/Views/ManualAddVet.dart';
import 'package:flutter/material.dart';

class AddVet extends StatefulWidget {
  @override
  _AddVetState createState() => _AddVetState();
}

class _AddVetState extends State<AddVet> {
  List<String> searchTypeList = ["By Name", "By Place", "By Ratings"];
  String _selectedSearchType;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Constants().greyColorAppBar,
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Constants().fontBlack,
            )),
        elevation: 0,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 10, left: 20),
              width: MediaQuery.of(context).size.width,
              color: Constants().greyColorAppBar,
              child: Text(
                "Add a Vet",
                style: TextStyle(
                    fontFamily: "AxiformaBold",
                    color: Constants().blue,
                    fontSize: Constants().fontSize20 + 2),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Pet Name",
                        style: TextStyle(
                            fontFamily: "AxiformaBold",
                            color: Constants().fontBlack),
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 5)),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(0.0, 5.0), //(x,y)
                                  blurRadius: 15.0,
                                )
                              ]),
                          child: TextField(
                            decoration: InputDecoration(
                                hintText: "Enter Name",
                                border: InputBorder.none),
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 20)),
                      Text(
                        "Search for Your Veterinary Clinic",
                        style: TextStyle(
                            fontFamily: "AxiformaBold",
                            color: Constants().fontBlack,
                            fontSize: Constants().fontSize18),
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Text(
                        "Search By",
                        style: TextStyle(
                            fontFamily: "AxiformaBold",
                            color: Constants().fontBlack),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(0.0, 5.0), //(x,y)
                                    blurRadius: 15.0,
                                  )
                                ]),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                // underline: SizedBox(),

                                hint: Text('Select an option',
                                    style: TextStyle(
                                        fontSize:
                                            15)), // Not necessary for Option 1
                                value: _selectedSearchType,
                                onChanged: (newValue) {
                                  setState(() {
                                    _selectedSearchType = newValue;
                                  });
                                },
                                items: searchTypeList.map((location) {
                                  return DropdownMenuItem(
                                    child: new Text(location),
                                    value: location,
                                  );
                                }).toList(),
                              ),
                            )),
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Text(
                        "Clinic Name",
                        style: TextStyle(
                            fontFamily: "AxiformaBold",
                            color: Constants().fontBlack),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                              width: MediaQuery.of(context).size.width / 1.7,
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey,
                                      offset: Offset(0.0, 5.0), //(x,y)
                                      blurRadius: 15.0,
                                    )
                                  ]),
                              child: TextField(
                                decoration: InputDecoration(
                                    hintText: "Enter Name",
                                    border: InputBorder.none),
                              )),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 12, horizontal: 20),
                            decoration: BoxDecoration(
                                color: Constants().searchBtnClr,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(0.0, 5.0), //(x,y)
                                    blurRadius: 15.0,
                                  )
                                ]),
                            child: Text("Search",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "AxiformaBold",
                                    fontSize: Constants().fontSize15)),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 20)),
                      GestureDetector(
                        onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ManualAddVet())),
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 15),
                          decoration: BoxDecoration(
                            color: Constants().greyColorAppBar,
                            borderRadius: BorderRadius.all(
                              Radius.circular(5),
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Can’t find your vet?",
                                  style: TextStyle(
                                    color: Constants().fontBlack,
                                    fontFamily: "AxiformaRegular",
                                  )),
                              Text(" Enter Manually",
                                  style: TextStyle(
                                    color: Constants().fontOrange,
                                    decoration: TextDecoration.underline,
                                    fontFamily: "AxiformaRegular",
                                  )),
                            ],
                          ),
                        ),
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: 5,
                          itemBuilder: (BuildContext context, int index) {
                            return Column(
                              children: <Widget>[
                                Padding(padding: EdgeInsets.only(bottom: 10)),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 15, horizontal: 10),
                                  decoration: BoxDecoration(
                                    color: Constants().greyColorAppBar,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(5),
                                    ),
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "STRAY PETS IN NEED OF MA INC",
                                        style: TextStyle(
                                            fontFamily: "AxiformaBold",
                                            color: Constants().fontBlack),
                                      ),
                                      Text(
                                        "781-235-1218",
                                        style: TextStyle(
                                            fontFamily: "AxiformaBold",
                                            color: Constants().headingsLight,
                                            fontSize: Constants().fontSize13),
                                      ),
                                      Text(
                                        "5 Overbrook Ter, Natick, MA 01760",
                                        style: TextStyle(
                                            fontFamily: "AxiformaBold",
                                            color: Constants().headingsLight,
                                            fontSize: Constants().fontSize13),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            );
                          })
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
