import 'package:app/Utills/constants.dart';
import 'package:flutter/material.dart';

class MultiSelectChip extends StatefulWidget {
  final bool isHead;
  final List<String> reportList;
  final Function(List<String>) onSelectionChanged; // +added
  MultiSelectChip(this.reportList, this.isHead,
      {this.onSelectionChanged} // +added
      );
  @override
  _MultiSelectChipState createState() => _MultiSelectChipState();
}

class _MultiSelectChipState extends State<MultiSelectChip> {
  // String selectedChoice = "";
  List<String> selectedChoices = new List();
  _buildChoiceList() {
    List<Widget> choices = List();
   for(int i=0;i<widget.reportList.length;i++){
   
      choices.add(Container(
        padding: const EdgeInsets.all(2.0),
        child: ChipTheme(
          data: ChipTheme.of(context).copyWith(
            //labelStyle: TextStyle(color: Colors.black),
            secondaryLabelStyle: TextStyle(color: Colors.black),
            //selectedColor: Colors.black,

            secondarySelectedColor: Color(0xFF2a2861),
            //brightness: Brightness.light
          ),
          child: ChoiceChip(
            label: Text(widget.reportList[i]),
            selected: selectedChoices.contains(widget.reportList[i]),
            backgroundColor: Colors.grey[300],
            //disabledColor: Colors.grey,
            labelStyle: TextStyle(
              color: selectedChoices.contains(widget.reportList[i])
                  ? Colors.white
                  : Constants().fontBlack,
            ),
            onSelected: (selected) {
              setState(() {
                selectedChoices.contains(widget.reportList[i])
                    ? selectedChoices.remove(widget.reportList[i])
                    : selectedChoices.add(widget.reportList[i]);
                //  widget.onSelectionChanged(selectedChoices); // +added
                print(selectedChoices);
              });
            },
          ),
        ),
      ));
    }
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: _buildChoiceList(),
    );
  }
}
