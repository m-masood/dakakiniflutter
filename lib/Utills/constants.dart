import 'package:app/Views/CartScreen.dart';
import 'package:flutter/material.dart';


const colorMain = Color(0xffBB872C);
double btnHeight = 45.0;
double editTxtHeight = 75.0;
const colorDivider = Color(0xffE1E1E1);
const smokeybgColor = Color(0xffEFEFEF);

const upperRoundedCorners= BorderRadius.only(topLeft: Radius.circular(40),topRight: Radius.circular(40));

String dummytxt = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.";

dummyCoverImage(context, isLoading) {
  return Container(
      color: colorDivider,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            isLoading ? "Loading..." : "No Image",
            style: TextStyle(fontSize: 18.0, letterSpacing: 1.0),
          ),
          SizedBox(
            height: 10,
          ),
          RichText(
            text: new TextSpan(
              // Note: Styles for TextSpans must be explicitly defined.
              // Child text spans will inherit styles from parent
              style: new TextStyle(
                fontSize: 12.0,
                color: Colors.black,
              ),
              children: <TextSpan>[
                new TextSpan(
                    text: 'Copyright © 2020 ',
                    style: TextStyle(fontWeight: FontWeight.bold)),
                new TextSpan(
                    text: 'Brokers Circle',
                    style: new TextStyle(fontWeight: FontWeight.bold)),
              ],
            ),
          ),
          Text(
            "www.brokerscircle.net",
            style: TextStyle(fontSize: 12.0),
          ),
        ],
      ));
}
actionWidgets(context) {
  return <Widget>[
    Padding(
      padding: const EdgeInsets.only(left:10.0,right: 10),
      child: GestureDetector(
        onTap: (){
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      CartScreen()));
        },
        child: Image.asset(
          "Assets/cart.png",
          height: 27,
          width: 27,
          color: Colors.white,
        ),
      ),
    ),
    GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, 'settings');
      },
      child: CircleAvatar(
        radius: 28,
        backgroundColor: Colors.transparent,
        // radius: 45.0,
        child: ClipOval(
          child: Container(
            color: Colors.indigoAccent,
            child: Image.asset("Assets/avatar.png",
              fit: BoxFit.cover, height: 34, width: 34,),
          ),
        ),
      ),
    ),
  ];
}
noImageAvailable({height, width}) {
  return Container(
    color: Color(0xffd8d6d9),
    child: Image.asset(
      "assets/no_image.png",
      height: height,
      width: width,
    ),
  );
}

noImageAvailableForUser({height, width}) {
  return Container(
      padding: EdgeInsets.only(
        top: 5,
      ),
      width: width,
      height: height,
      color: Colors.grey[200],
      child:
      new Image.asset("assets/no_profile_image.png", color: Colors.white));
}

void showInSnackBar(String value, context) {
  Scaffold.of(context).showSnackBar(new SnackBar(content: new Text(value)));
}

customeAppbar(text,context){
  return  AppBar(
  brightness: Brightness.light,
  elevation: 0,
  backgroundColor: colorMain,
  title: Text(text,style: TextStyle(
      color: Colors.white,
      fontFamily: "AxiformaBold",
      fontSize: Constants().fontSize18,
  ),),
  titleSpacing: 0.0,
  leading: GestureDetector(
  onTap: () {
  Navigator.pop(context);
  },
  child: Icon(
  Icons.arrow_back,
  color: Colors.white,
  )),
  actions: actionWidgets(context),
  );
}
//computeTimeAndDate(dateAndTime) {
//  var computedTime;
//  if ((DateTime.now().month == dateAndTime.month) &&
//      (DateTime.now().year == dateAndTime.year))
//    computedTime = timeago.format(
//        DateTime.now().subtract(Duration(
//            minutes: dateAndTime.minute,
//            seconds: dateAndTime.second,
//            hours: dateAndTime.hour)),
//        locale: 'en');
//  else
//    computedTime = DateTimeUtils.getFullDate(dateAndTime).toString();
//  return computedTime;
//}
class Constants {
  Color blue = const Color(0xFF2a2861);
  Color orange = const Color(0xFFfb876e);
  Color fontBlack = const Color(0xFF464648);
  Color fontOrange = const Color(0xFFeb421b);
  Color gradientStart = const Color(0xFF4c41a3);
  Color gradientEnd = const Color(0xFF1f176f);
  Color buttonLightBlue = const Color(0xFF5c51b2);
  Color percentageOrange = const Color(0xFFf49475);
  Color lsBLUE = const Color(0xFF453b9b);
  Color greyColorLabel = const Color(0xFFb5b5b6);
  Color greyColorAppBar = const Color(0xFFeeeef0);
  Color appbarTextclr = const Color(0xFF4a426f);
  Color tabBarUNFOCUSclr = const Color(0xFF9e99c8);
  Color headingsLight = const Color(0xFF888888);
  Color cardLgrey = const Color(0xFFf5f5f5);
  Color green = const Color(0xFF17c228);
  Color dismisTxtClr = const Color(0xFFf49475);
  Color starColor = const Color(0xFFffaa00);
  Color pricTagColor = const Color(0xFFef8f79);
  Color hyperLinkColor = const Color(0xFF3947dc);
  Color radioBTnClr = const Color(0xFFeb5d3b);
  Color searchBtnClr = const Color(0xFF5d5781);

  Color colorMain = const Color(0xFF33333);
  // Color  = const Color(0xFFBCB5B9);
  double fontSize32 = 35;
  double fontSize25 = 25;
  double fontSize20 = 20;
  double fontSize18 = 18;
  double fontSize15 = 15;
  double fontSize13 = 13;
  double fontSize11 = 11;


}
