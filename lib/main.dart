import 'dart:async';
import 'package:app/Utills/constants.dart';
import 'package:app/Views/LoginSreen.dart';
import 'package:app/Views/MainBottomBar.dart';
import 'package:app/Views/Shop/ShopProducts.dart';
import 'package:app/Views/SideNavigation/MyOrdersScreen.dart';
import 'package:app/Views/SideNavigation/NotificationScreen.dart';
import 'package:app/Views/SideNavigation/settings/SettingsScreen.dart';
import 'package:app/Views/SignupScreen.dart';
import 'package:app/Views/SplashScreen.dart';
import 'package:app/Views/dashboardNtabs/OrdersTab.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  Crashlytics.instance.enableInDevMode = true;

  // Pass all uncaught errors to Crashlytics.
  FlutterError.onError = Crashlytics.instance.recordFlutterError;

  runZoned(() { 
    runApp(MyApp());
  }, onError: Crashlytics.instance.recordError);
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: colorDivider));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',

      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: 'splash',
      routes: {
        'splash':(context)=>Splashscreen(),
        'login':(context)=>LoginScreen(),
        'signup':(context)=>SignupScreen(),
        'home':(context)=>MyHomePage(),
        'settings':(context)=>SettingsScreen(),
        'notifications':(context)=>NotificationsScreen(),
        'myorders':(context)=>MyOrderScreen(),
      //  'profile':(context)=>SettingsScreen(),



      },
      home: Splashscreen(),
      // home: HomeScreen(title: 'Flutter Demo Home Page'),
    );
  }
}
